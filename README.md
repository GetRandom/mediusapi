### 1. Клонировать репозиторий ###
```
#!bash
git clone https://bitbucket.org/GetRandom/medius.git
```

### 2. Установить зависимости ###
```
#!bash
php composer.phar self-update
php composer.phar install
```

### 3. Сконфигурировать модули ###

3.1. Переместить файл с переименованием
```
#!path
vendor/zendframework/zend-developer-tools/config/zenddevelopertools.local.php.dist
```
в
```
#!path
config/autoload/zenddevelopertools.local.php
```

3.2. Создать файл с параметрами доступа к БД, который будет использоваться Doctrine
```
#!path
config/autoload/doctrine.local.php
```
Пример содержимого файла:
```
#!PHP
<?php
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' =>'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host'     => 'localhost',
                    'port'     => '3306',
                    'user'     => 'root',
                    'password' => '',
                    'dbname'   => 'hosp',
                    'charset' => 'utf8',
                    'driverOptions' => array(
                        1002 => 'SET NAMES utf8',
                    ),
                )
            )
        ),
    ),
);
```

3.3. Создать файл разрешениями для модуля BjyAuthorize
```
#!path
config/autoload/bjyauth.local.php
```
Пример содержимого:
(Разрешаем доступ к дев. модулю всем ролям)
```
#!php
<?php
// Access control for ZFTool module
return array(
    'bjyauthorize' => array(
        'guards' => array(
            'BjyAuthorize\Guard\Controller' => array(
                array(
                    'controller' => array(
                            'ZFTool\Controller\Module',
                            'ZFTool\Controller\Create'
                        ),
                    'roles' => array()
                ),
            )
        )
    )
);

```

### 4. Создание виртуального хоста ###

4.1. Добавить в файл

```
#!path
path/to/xampp/apache/conf/extra/httpd-vhosts.conf
```
Пример содержимого
```
#!ini
    <VirtualHost *:80>
        ServerName medius.loc
        DocumentRoot /path/to/medius/public
        <Directory /path/to/medius/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>
```

4.2. (Для Windows) Добавить в файл
```
#!path
<Disk>:\Windows\System32\drivers\etc\hosts
```
строку
```
#!cfg
127.0.0.1	medius.loc
```

### 5. Создание базы данных ###

5.1. Создать БД
```
#!sql
CREATE SCHEMA `hosp` DEFAULT CHARACTER SET utf8 ;
```
5.2. Forward Engineering
Открыть файл medius.mwb и выполнить Database -> Forward Engineering

-------------------------------------------------------------------------------------------------------------
(Не выполнять) Разные заметки:
```
#!bash

./vendor/doctrine/doctrine-module/bin/doctrine-module orm:convert-mapping --namespace="Medcard\\Entity\\" --force  --from-database annotation ./module/Medcard/src/
```
```
#!bash

./vendor/doctrine/doctrine-module/bin/doctrine-module orm:generate-entities ./module/Medcard/src/ --generate-annotations=true
```