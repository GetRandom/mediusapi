<?php

return array(
    'router' => array(
        'routes' => array(
            // Staff
            'staff' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/staff[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Staff',
                        'action' => 'index',
                    ),
                ),
            ),
            'staff_login' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/staff/login',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Staff',
                        'action' => 'login',
                    ),
                ),
            ),
            // Medcard
            'medcard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/medcard[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Medcard',
                        'action' => 'index',
                    ),
                ),
            ),
            // Index
            'home' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
            // ICD
            'icd_route' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/icd[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Icd',
                        'action' => 'index',
                    ),
                ),
            ),
            // Administration
            'treatment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/administration[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Administration',
                        'action' => 'index',
                    ),
                ),
            ),
            // Help
            'help' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/help[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Help',
                        'action' => 'index',
                    ),
                ),
            ),
            // NurseWorkplace
            'nurse_workplace' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/nurse-workplace',
                    'defaults' => array(
                        'controller' => 'Application\Controller\NurseWorkplace',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Staff' => 'Application\Controller\StaffController',
            'Application\Controller\Medcard' => 'Application\Controller\MedcardController',
            'Application\Controller\Icd' => 'Application\Controller\IcdController',
            'Application\Controller\Administration' => 'Application\Controller\AdministrationController',
            'Application\Controller\Help' => 'Application\Controller\HelpController',
            'Application\Controller\NurseWorkplace' => 'Application\Controller\NurseWorkplaceController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'bjyauthorize' => array(
        'guards' => array(
            'BjyAuthorize\Guard\Controller' => array(
                array(
                    'controller' => 'Application\Controller\Index',
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'Application\Controller\Staff',
                    'action' => array('login'),
                    'roles' => array()
                ),
                array(
                    'controller' => 'Application\Controller\Staff',
                    'roles' => array('administrator')
                ),
                
                array(
                    'controller' => 'Application\Controller\Medcard',
                    'roles' => array('doctor', 'nurse')
                ),
                array(
                    'controller' => 'Application\Controller\Icd',
                    'roles' => array()
                ),
                array(
                    'controller' => 'Application\Controller\Administration',
                    'roles' => array('administrator')
                ),
                array(
                    'controller' => 'Application\Controller\Help',
                    'roles' => array()
                ),
                array(
                    'controller' => 'Application\Controller\NurseWorkplace',
                    'roles' => array('nurse')
                ),
            ),
        ),
    ),
);
