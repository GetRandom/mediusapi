<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class StaffController extends AbstractActionController {

    public function indexAction() {
        return new ViewModel();
    }

    public function addAction() {
        return new ViewModel();
    }

    public function viewAction() {
        return new ViewModel();
    }

    public function removeAction() {
        return new ViewModel();
    }

    public function loginAction() {
        $authService = $this->getServiceLocator()->get('Staff\Service\AuthService');
        if ($authService->hasIdentity()) {
            $identity = $authService->getIdentity();
            $roles = $identity->getRoles();
            foreach ($roles as $role) {
                if ($role->getRoleTitle() == 'administrator') {
                    $this->redirect()->toRoute('staff');
                } else {
                    $this->redirect()->toRoute('medcard');
                }
            }
        }
        return (new ViewModel())->setTerminal(true);
    }

}
