<?php

return array(
    'router' => array(
        'routes' => array(
            'api_common' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/:controller/:action[/:id]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'medcard' => 'Common\Factory\MedcardControllerFactory',
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'common_entity' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/Common/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Common\Entity' => 'common_entity',
                ),
            ),
        ),
        'connection' => array(
            'orm_default' => array(
                'doctrine_type_mappings' => array(
                    'enum' => 'string'
                ),
            ),
        ),
    ),
    'bjyauthorize' => array(
        'guards' => array(
            'BjyAuthorize\Guard\Controller' => array(
                /* MEDCARD */
                array(
                    'controller' => 'medcard',
                    'action' => array('all', 'view'),
                    'roles' => array('nurse')
                ),
                array(
                    'controller' => 'medcard',
                    'roles' => array('doctor', 'administrator')
                ),
            ),
        ),
    ),
);
