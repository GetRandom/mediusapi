<?php

namespace Common\Controller;

use Zend\Mvc\Controller\AbstractActionController;

abstract class AbstractMediusController extends AbstractActionController {

// PUBLIC:
    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
    }

    public function setRepository($repository) {
        $this->repository = $repository;
    }
    
    public function setHydrator($hydrator) {
        $this->hydrator = $hydrator;
    }

    public function getJsonContent() {
        return json_decode($this->getRequest()->getContent(), true);
    }

// PROTECTED:
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;
    protected $hydrator;
    protected $repository;
}
