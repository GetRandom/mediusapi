<?php

return array(
    'router' => array(
        'routes' => array(
            'api_icd' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/:controller/:action[/:id]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'icd' => 'ICD\Factory\IcdControllerFactory',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'icd_entity' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/ICD/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'ICD\Entity' => 'icd_entity',
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'bjyauthorize' => array(
        'guards' => array(
            'BjyAuthorize\Guard\Controller' => array(
                array(
                    'controller' => 'icd',
                    'roles' => array()
                ),
            ),
        ),
    ),
);
