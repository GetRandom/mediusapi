<?php

namespace ICD\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Block
 *
 * @ORM\Table(name="block", uniqueConstraints={@ORM\UniqueConstraint(name="Num_Of_Block_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_block_icd_class1_idx", columns={"icd_class_id"})})
 * @ORM\Entity
 */
class Block
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=10, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var \ICD\Entity\IcdClass
     *
     * @ORM\ManyToOne(targetEntity="ICD\Entity\IcdClass")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="icd_class_id", referencedColumnName="id")
     * })
     */
    private $icdClass;



    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Block
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set icdClass
     *
     * @param \ICD\Entity\IcdClass $icdClass
     * @return Block
     */
    public function setIcdClass(\ICD\Entity\IcdClass $icdClass = null)
    {
        $this->icdClass = $icdClass;

        return $this;
    }

    /**
     * Get icdClass
     *
     * @return \ICD\Entity\IcdClass 
     */
    public function getIcdClass()
    {
        return $this->icdClass;
    }
}
