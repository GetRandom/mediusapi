<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class ContractController extends AbstractActionController {

// PUBLIC:
    const ENTITY_NAME = '\Medcard\Entity\Contract';

    public function allAction() {
        $data = $this->entityManager->createQuery('SELECT e FROM ' . self::ENTITY_NAME . ' e')->getArrayResult();
        return new JsonModel($data);
    }

    public function viewAction() {
        $id = $this->params('id');
        $data = $this->entityManager->createQuery('SELECT e FROM ' . self::ENTITY_NAME . ' e WHERE e.id = ?1')
                        ->setParameter(1, $id)->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        return new JsonModel($data);
    }

    public function addAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Contract());
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function updateAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Contract());
            $this->entityManager->merge($object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function removeAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Contract());
            $this->entityManager->merge($object);
            $this->entityManager->remove($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($this->entityManager, self::ENTITY_NAME);
    }

// PROTECTED:
    protected $entityManager;
    protected $hydrator;

    protected function getJsonContent() {
        return json_decode($this->getRequest()->getContent(), true);
    }

}
