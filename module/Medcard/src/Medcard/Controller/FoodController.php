<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class FoodController extends AbstractActionController {

    const ENTITY_NAME = '\Medcard\Entity\Food';
    
    protected $entityManager;
    protected $hydrator;

    protected function getJsonContent() {
        return json_decode($this->getRequest()->getContent(), true);
    }

    public function allAction() {
        $data = $this->entityManager->createQuery('SELECT c FROM '. self::ENTITY_NAME .' c')->getArrayResult();
        return new JsonModel($data);
    }
    
    public function viewAction() {
        $id = $this->params('id');
        $data = $this->entityManager->createQuery('SELECT c FROM ' . self::ENTITY_NAME . ' c WHERE c.id = ?1')
                ->setParameter(1, $id)->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        return new JsonModel($data);
    }
    
    public function addAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Food());
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }
    
    public function updateAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Food());
            $this->entityManager->merge($object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }
    
    public function removeAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Food());
            $this->entityManager->merge($object);
            $this->entityManager->remove($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }
    
    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($this->entityManager, self::ENTITY_NAME);
    }

}
