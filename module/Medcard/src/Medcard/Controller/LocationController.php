<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class LocationController extends AbstractActionController {

// PUBLIC:
    const ENTITY_NAME = '\Medcard\Entity\Location';

    public function viewAction() {
        $data = [];
        $id = $this->params()->fromRoute('id');
        $object = $this->entityManager->getRepository(self::ENTITY_NAME)->findOneBy(array('patient' => $id));
        if ($object) {
            $data = $this->hydrator->extract($object);
            $data['patient'] = $object->getPatient()->getId();
            if ($object->getLocality()) {
                $data['locality'] = $object->getLocality()->getId();
            } else {
                $data['locality'] = null;
            }
        }
        return new JsonModel($data);
    }

    public function updateAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Location());
            $this->entityManager->merge($object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($this->entityManager, self::ENTITY_NAME);
    }

// PROTECTED:
    protected $entityManager;
    protected $hydrator;

}
