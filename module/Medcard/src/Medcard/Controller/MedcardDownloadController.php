<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class MedcardDownloadController extends AbstractActionController {

    const ENTITY_NAME = '\Common\Entity\Medcard';

// PUBLIC:
    public function downloadAction() {
        $id = $this->params('id');
        $medcard = $this->entityManager->getRepository(self::ENTITY_NAME)->find($id);

        $medcardBuilder = $this->getServiceLocator()->get('\Medcard\Service\MedcardBuildingService');
        $document = $medcardBuilder->buildMedcard($medcard);
        $name = 'medcard_' . $medcard->getId() . '.docx';
        $path = 'data/temp/' . $name;
        $document->saveAs($path);

        $headers = new \Zend\Http\Headers();
        $headers->addHeaderLine('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $name . '"')
                ->addHeaderLine('Content-Length', filesize($path));

        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen($path, 'r'));
        $response->setStatusCode(200);
        $response->setHeaders($headers);

        return $response;
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($entityManager, self::ENTITY_NAME);
    }

// PROTECTED:
    protected $entityManager;
    protected $hydrator;

    protected function getJsonContent() {
        return json_decode($this->getRequest()->getContent(), true);
    }

}
