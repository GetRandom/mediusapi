<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class NotebookController extends AbstractActionController {

// PUBLIC:
    const ENTITY_NAME = '\Medcard\Entity\Notebook';

    public function allAction() {
        $treatmentId = $this->params('id');
        $data = $this->entityManager->createQuery('SELECT c FROM ' . self::ENTITY_NAME . ' c WHERE c.treatment = ?1')
                        ->setParameter(1, $treatmentId)->getArrayResult();
        return new JsonModel($data);
    }

    public function updateAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Notebook());
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function removeAction() {
        if ($this->getRequest()->isPost()) {
            $id = $this->getJsonContent()['id'];
            $this->entityManager->createQuery('DELETE FROM ' . self::ENTITY_NAME . ' o WHERE o.id = ?1')
                    ->setParameter(1, $id)->execute();
        }
        return new JsonModel();
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($this->entityManager, self::ENTITY_NAME);
    }

// PROTECTED:
    protected $entityManager;
    protected $hydrator;

    protected function getJsonContent() {
        return json_decode($this->getRequest()->getContent(), true);
    }

}
