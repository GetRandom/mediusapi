<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class RegionController extends AbstractActionController {

// PUBLIC:
    const ENTITY_NAME = '\Medcard\Entity\Region';

    public function allAction() {
        $data = $this->entityManager->createQuery('SELECT e FROM ' . self::ENTITY_NAME . ' e')->getArrayResult();
        return new JsonModel($data);
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($this->entityManager, self::ENTITY_NAME);
    }

// PROTECTED:
    protected $entityManager;
    protected $hydrator;

}
