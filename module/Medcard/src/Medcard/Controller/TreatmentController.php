<?php

namespace Medcard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class TreatmentController extends AbstractActionController {

// PUBLIC:
    const ENTITY_NAME = '\Medcard\Entity\Treatment';

    public function viewAction() {
        $id = $this->params()->fromRoute('id');
        $data = $this->entityManager->createQuery('SELECT e FROM ' . self::ENTITY_NAME . ' e WHERE e.medcard = ?1')
                        ->setParameter(1, $id)->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        $object = $this->entityManager->getRepository(self::ENTITY_NAME)->findOneBy(['medcard' => $id]);
        $data['contract'] = $object->getContract() ? $object->getContract()->getId() : null;
        return new JsonModel($data);
    }

    public function updateAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getJsonContent();
            $object = $this->hydrator->hydrate($data, new \Medcard\Entity\Treatment);
            $this->entityManager->merge($object);
            $this->entityManager->persist($object);
            $this->entityManager->flush();
        }
        return new JsonModel();
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
        $this->hydrator = new DoctrineHydrator($this->entityManager, self::ENTITY_NAME);
    }

// PROTECTED:
    protected $entityManager;
    protected $hydrator;

    protected function getJsonContent() {
        return json_decode($this->getRequest()->getContent(), true);
    }

}
