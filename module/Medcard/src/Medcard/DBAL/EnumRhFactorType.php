<?php
namespace Medcard\DBAL;

class EnumRhFactorType extends EnumType
{
    protected $name = 'enumrhfactor';
    protected $values = array('+', '-');
}