<?php

namespace Medcard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdmissionDepartment
 *
 * @ORM\Table(name="admission_department", uniqueConstraints={@ORM\UniqueConstraint(name="treatment_id_UNIQUE", columns={"treatment_id"})})
 * @ORM\Entity
 */
class AdmissionDepartment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="complaint", type="text", nullable=true)
     */
    private $complaint;

    /**
     * @var string
     *
     * @ORM\Column(name="disease_anamnesis", type="text", nullable=true)
     */
    private $diseaseAnamnesis;

    /**
     * @var string
     *
     * @ORM\Column(name="life_anamnesis", type="text", nullable=true)
     */
    private $lifeAnamnesis;

    /**
     * @var string
     *
     * @ORM\Column(name="objective_state", type="text", nullable=true)
     */
    private $objectiveState;

    /**
     * @var string
     *
     * @ORM\Column(name="other", type="text", nullable=true)
     */
    private $other;

    /**
     * @var \Medcard\Entity\Treatment
     *
     * @ORM\ManyToOne(targetEntity="Medcard\Entity\Treatment", inversedBy="admissionDepartment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="treatment_id", referencedColumnName="id")
     * })
     */
    private $treatment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set complaint
     *
     * @param string $complaint
     * @return AdmissionDepartment
     */
    public function setComplaint($complaint)
    {
        $this->complaint = $complaint;

        return $this;
    }

    /**
     * Get complaint
     *
     * @return string 
     */
    public function getComplaint()
    {
        return $this->complaint;
    }

    /**
     * Set diseaseAnamnesis
     *
     * @param string $diseaseAnamnesis
     * @return AdmissionDepartment
     */
    public function setDiseaseAnamnesis($diseaseAnamnesis)
    {
        $this->diseaseAnamnesis = $diseaseAnamnesis;

        return $this;
    }

    /**
     * Get diseaseAnamnesis
     *
     * @return string 
     */
    public function getDiseaseAnamnesis()
    {
        return $this->diseaseAnamnesis;
    }

    /**
     * Set lifeAnamnesis
     *
     * @param string $lifeAnamnesis
     * @return AdmissionDepartment
     */
    public function setLifeAnamnesis($lifeAnamnesis)
    {
        $this->lifeAnamnesis = $lifeAnamnesis;

        return $this;
    }

    /**
     * Get lifeAnamnesis
     *
     * @return string 
     */
    public function getLifeAnamnesis()
    {
        return $this->lifeAnamnesis;
    }

    /**
     * Set objectiveState
     *
     * @param string $objectiveState
     * @return AdmissionDepartment
     */
    public function setObjectiveState($objectiveState)
    {
        $this->objectiveState = $objectiveState;

        return $this;
    }

    /**
     * Get objectiveState
     *
     * @return string 
     */
    public function getObjectiveState()
    {
        return $this->objectiveState;
    }

    /**
     * Set other
     *
     * @param string $other
     * @return AdmissionDepartment
     */
    public function setOther($other)
    {
        $this->other = $other;

        return $this;
    }

    /**
     * Get other
     *
     * @return string 
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * Set treatment
     *
     * @param \Medcard\Entity\Treatment $treatment
     * @return AdmissionDepartment
     */
    public function setTreatment(\Medcard\Entity\Treatment $treatment = null)
    {
        $this->treatment = $treatment;

        return $this;
    }

    /**
     * Get treatment
     *
     * @return \Medcard\Entity\Treatment 
     */
    public function getTreatment()
    {
        return $this->treatment;
    }
}
