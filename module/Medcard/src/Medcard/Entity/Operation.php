<?php

namespace Medcard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Operation
 *
 * @ORM\Table(name="operation", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_operation_treatment1_idx", columns={"treatment_id"})})
 * @ORM\Entity
 */
class Operation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=true)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="anesthesia_method", type="string", length=100, nullable=true)
     */
    private $anesthesiaMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="complications", type="string", length=100, nullable=true)
     */
    private $complications;

    /**
     * @var string
     *
     * @ORM\Column(name="surgeon", type="string", length=100, nullable=true)
     */
    private $surgeon;

    /**
     * @var string
     *
     * @ORM\Column(name="anesthetist", type="string", length=100, nullable=true)
     */
    private $anesthetist;

    /**
     * @var \Medcard\Entity\Treatment
     *
     * @ORM\ManyToOne(targetEntity="Medcard\Entity\Treatment", inversedBy="operations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="treatment_id", referencedColumnName="id")
     * })
     */
    private $treatment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Operation
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Operation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Operation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set anesthesiaMethod
     *
     * @param string $anesthesiaMethod
     * @return Operation
     */
    public function setAnesthesiaMethod($anesthesiaMethod)
    {
        $this->anesthesiaMethod = $anesthesiaMethod;

        return $this;
    }

    /**
     * Get anesthesiaMethod
     *
     * @return string 
     */
    public function getAnesthesiaMethod()
    {
        return $this->anesthesiaMethod;
    }

    /**
     * Set complications
     *
     * @param string $complications
     * @return Operation
     */
    public function setComplications($complications)
    {
        $this->complications = $complications;

        return $this;
    }

    /**
     * Get complications
     *
     * @return string 
     */
    public function getComplications()
    {
        return $this->complications;
    }

    /**
     * Set surgeon
     *
     * @param string $surgeon
     * @return Operation
     */
    public function setSurgeon($surgeon)
    {
        $this->surgeon = $surgeon;

        return $this;
    }

    /**
     * Get surgeon
     *
     * @return string 
     */
    public function getSurgeon()
    {
        return $this->surgeon;
    }

    /**
     * Set anesthetist
     *
     * @param string $anesthetist
     * @return Operation
     */
    public function setAnesthetist($anesthetist)
    {
        $this->anesthetist = $anesthetist;

        return $this;
    }

    /**
     * Get anesthetist
     *
     * @return string 
     */
    public function getAnesthetist()
    {
        return $this->anesthetist;
    }

    /**
     * Set treatment
     *
     * @param \Medcard\Entity\Treatment $treatment
     * @return Operation
     */
    public function setTreatment(\Medcard\Entity\Treatment $treatment = null)
    {
        $this->treatment = $treatment;

        return $this;
    }

    /**
     * Get treatment
     *
     * @return \Medcard\Entity\Treatment 
     */
    public function getTreatment()
    {
        return $this->treatment;
    }
}
