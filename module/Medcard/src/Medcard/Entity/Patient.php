<?php

namespace Medcard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Patient
 *
 * @ORM\Table(name="patient", uniqueConstraints={@ORM\UniqueConstraint(name="medcard_id_UNIQUE", columns={"medcard_id"})}, indexes={@ORM\Index(name="fk_patient_category1_idx", columns={"category_id"})})
 * @ORM\Entity
 */
class Patient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=45, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="patronymic", type="string", length=45, nullable=true)
     */
    private $patronymic;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", nullable=true)
     */
    private $sex;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="born", type="date", nullable=true)
     */
    private $born;

    /**
     * @var enumblood
     *
     * @ORM\Column(name="blood_type", type="enumblood", nullable=true)
     */
    private $bloodType;

    /**
     * @var enumrhfactor
     *
     * @ORM\Column(name="rh_factor", type="enumrhfactor", nullable=true)
     */
    private $rhFactor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rw", type="date", nullable=true)
     */
    private $rw;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hiv_infection", type="date", nullable=true)
     */
    private $hivInfection;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="rw_status", type="boolean", nullable=true)
     */
    private $rwStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hiv_status", type="boolean", nullable=true)
     */
    private $hivStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="sensitivity", type="string", length=100, nullable=true)
     */
    private $sensitivity;

    /**
     * @var string
     *
     * @ORM\Column(name="workplace", type="string", length=100, nullable=true)
     */
    private $workplace;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=16, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="parents_address", type="string", length=100, nullable=true)
     */
    private $parentsAddress;

    /**
     * @var \Medcard\Entity\Category
     *
     * @ORM\ManyToOne(targetEntity="Medcard\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \Common\Entity\Medcard
     *
     * @ORM\OneToOne(targetEntity="Common\Entity\Medcard", inversedBy="patient")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="medcard_id", referencedColumnName="id")
     * })
     */
    private $medcard;

    /**
     * @ORM\OneToOne(targetEntity="Medcard\Entity\Location", mappedBy="patient")
     */
    private $location;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Patient
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Patient
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set patronymic
     *
     * @param string $patronymic
     * @return Patient
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    /**
     * Get patronymic
     *
     * @return string 
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * Set sex
     *
     * @param string $sex
     * @return Patient
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return string 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set born
     *
     * @param \DateTime $born
     * @return Patient
     */
    public function setBorn($born)
    {
        $this->born = $born;

        return $this;
    }

    /**
     * Get born
     *
     * @return \DateTime 
     */
    public function getBorn()
    {
        return $this->born;
    }

    /**
     * Set bloodType
     *
     * @param enumblood $bloodType
     * @return Patient
     */
    public function setBloodType($bloodType)
    {
        $this->bloodType = $bloodType;

        return $this;
    }

    /**
     * Get bloodType
     *
     * @return enumblood 
     */
    public function getBloodType()
    {
        return $this->bloodType;
    }

    /**
     * Set rhFactor
     *
     * @param enumrhfactor $rhFactor
     * @return Patient
     */
    public function setRhFactor($rhFactor)
    {
        $this->rhFactor = $rhFactor;

        return $this;
    }

    /**
     * Get rhFactor
     *
     * @return enumrhfactor 
     */
    public function getRhFactor()
    {
        return $this->rhFactor;
    }

    /**
     * Set rw
     *
     * @param \DateTime $rw
     * @return Patient
     */
    public function setRw($rw)
    {
        $this->rw = $rw;

        return $this;
    }

    /**
     * Get rw
     *
     * @return \DateTime 
     */
    public function getRw()
    {
        return $this->rw;
    }

    /**
     * Set hivInfection
     *
     * @param \DateTime $hivInfection
     * @return Patient
     */
    public function setHivInfection($hivInfection)
    {
        $this->hivInfection = $hivInfection;

        return $this;
    }

    /**
     * Get hivInfection
     *
     * @return \DateTime 
     */
    public function getHivInfection()
    {
        return $this->hivInfection;
    }

    /**
     * Set rwStatus
     *
     * @param boolean $rwStatus
     * @return Patient
     */
    public function setRwStatus($rwStatus)
    {
        $this->rwStatus = $rwStatus;

        return $this;
    }

    /**
     * Get rwStatus
     *
     * @return boolean 
     */
    public function getRwStatus()
    {
        return $this->rwStatus;
    }

    /**
     * Set hivStatus
     *
     * @param boolean $hivStatus
     * @return Patient
     */
    public function setHivStatus($hivStatus)
    {
        $this->hivStatus = $hivStatus;

        return $this;
    }

    /**
     * Get hivStatus
     *
     * @return boolean 
     */
    public function getHivStatus()
    {
        return $this->hivStatus;
    }
    
    /**
     * Set sensitivity
     *
     * @param string $sensitivity
     * @return Patient
     */
    public function setSensitivity($sensitivity)
    {
        $this->sensitivity = $sensitivity;

        return $this;
    }

    /**
     * Get sensitivity
     *
     * @return string 
     */
    public function getSensitivity()
    {
        return $this->sensitivity;
    }

    /**
     * Set workplace
     *
     * @param string $workplace
     * @return Patient
     */
    public function setWorkplace($workplace)
    {
        $this->workplace = $workplace;

        return $this;
    }

    /**
     * Get workplace
     *
     * @return string 
     */
    public function getWorkplace()
    {
        return $this->workplace;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Patient
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set parentsAddress
     *
     * @param string $parentsAddress
     * @return Patient
     */
    public function setParentsAddress($parentsAddress)
    {
        $this->parentsAddress = $parentsAddress;

        return $this;
    }

    /**
     * Get parentsAddress
     *
     * @return string 
     */
    public function getParentsAddress()
    {
        return $this->parentsAddress;
    }

    /**
     * Set category
     *
     * @param \Medcard\Entity\Category $category
     * @return Patient
     */
    public function setCategory(\Medcard\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Medcard\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set medcard
     *
     * @param \Common\Entity\Medcard $medcard
     * @return Patient
     */
    public function setMedcard(\Common\Entity\Medcard $medcard = null)
    {
        $this->medcard = $medcard;

        return $this;
    }

    /**
     * Get medcard
     *
     * @return \Common\Entity\Medcard 
     */
    public function getMedcard()
    {
        return $this->medcard;
    }

    /**
     * Set location
     *
     * @param \Medcard\Entity\Location $location
     * @return Patient
     */
    public function setLocation(\Medcard\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \Medcard\Entity\Location 
     */
    public function getLocation()
    {
        return $this->location;
    }
}
