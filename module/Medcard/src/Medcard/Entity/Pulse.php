<?php

namespace Medcard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pulse
 *
 * @ORM\Table(name="pulse", indexes={@ORM\Index(name="fk_pulse_medcard1_idx", columns={"medcard_id"})})
 * @ORM\Entity
 */
class Pulse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer", nullable=true)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \Common\Entity\Medcard
     *
     * @ORM\ManyToOne(targetEntity="Common\Entity\Medcard")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="medcard_id", referencedColumnName="id")
     * })
     */
    private $medcard;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param integer $value
     * @return Pulse
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Pulse
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set medcard
     *
     * @param \Common\Entity\Medcard $medcard
     * @return Pulse
     */
    public function setMedcard(\Common\Entity\Medcard $medcard = null)
    {
        $this->medcard = $medcard;

        return $this;
    }

    /**
     * Get medcard
     *
     * @return \Common\Entity\Medcard 
     */
    public function getMedcard()
    {
        return $this->medcard;
    }
}
