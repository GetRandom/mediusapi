<?php

namespace Medcard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Treatment
 *
 * @ORM\Table(name="treatment", uniqueConstraints={@ORM\UniqueConstraint(name="medcard_id_UNIQUE", columns={"medcard_id"})}, indexes={@ORM\Index(name="fk_treatment_contract1_idx", columns={"contract_id"})})
 * @ORM\Entity
 */
class Treatment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="other_treatment", type="text", nullable=true)
     */
    private $otherTreatment;

    /**
     * @var string
     *
     * @ORM\Column(name="other_treatment_types", type="string", nullable=true)
     */
    private $otherTreatmentTypes;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="string", length=250, nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", nullable=true)
     */
    private $result;

    /**
     * @var string
     *
     * @ORM\Column(name="restored_efficiency", type="string", nullable=true)
     */
    private $restoredEfficiency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="oncological_observing", type="date", nullable=true)
     */
    private $oncologicalObserving;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="xray_observing", type="date", nullable=true)
     */
    private $xrayObserving;

    /**
     * @var \Medcard\Entity\Contract
     *
     * @ORM\ManyToOne(targetEntity="Medcard\Entity\Contract")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contract_id", referencedColumnName="id")
     * })
     */
    private $contract;

    /**
     * @var \Common\Entity\Medcard
     *
     * @ORM\OneToOne(targetEntity="Common\Entity\Medcard", inversedBy="treatment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="medcard_id", referencedColumnName="id")
     * })
     */
    private $medcard;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Medcard\Entity\Operation", mappedBy="treatment")
     */
    private $operations;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Medcard\Entity\ExaminationResult", mappedBy="treatment")
     */
    private $examinationResult;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Medcard\Entity\EfficiencyMarks", mappedBy="treatment")
     */
    private $efficiencyMarks;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Medcard\Entity\Notebook", mappedBy="treatment")
     */
    private $notebook;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Medcard\Entity\ExpertInspection", mappedBy="treatment")
     */
    private $expertInspection;
    
    /**
     * @var \Medcard\Entity\TreatmentDiagnosis
     *
     * @ORM\OneToOne(targetEntity="Medcard\Entity\TreatmentDiagnosis", mappedBy="treatment")
     */
    private $treatmentDiagnosis;
    
    /**
     * @var \Medcard\Entity\AdmissionDepartment
     *
     * @ORM\OneToOne(targetEntity="Medcard\Entity\AdmissionDepartment", mappedBy="treatment")
     */
    private $admissionDepartment;

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->operations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->examinationResult = new \Doctrine\Common\Collections\ArrayCollection();
        $this->efficiencyMarks = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notebook = new \Doctrine\Common\Collections\ArrayCollection();
        $this->expertInspection = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set otherTreatment
     *
     * @param string $otherTreatment
     * @return Treatment
     */
    public function setOtherTreatment($otherTreatment)
    {
        $this->otherTreatment = $otherTreatment;

        return $this;
    }

    /**
     * Get otherTreatment
     *
     * @return string 
     */
    public function getOtherTreatment()
    {
        return $this->otherTreatment;
    }

    /**
     * Set otherTreatmentTypes
     *
     * @param string $otherTreatmentTypes
     * @return Treatment
     */
    public function setOtherTreatmentTypes($otherTreatmentTypes)
    {
        $this->otherTreatmentTypes = $otherTreatmentTypes;

        return $this;
    }

    /**
     * Get otherTreatmentTypes
     *
     * @return string 
     */
    public function getOtherTreatmentTypes()
    {
        return $this->otherTreatmentTypes;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return Treatment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return Treatment
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string 
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set restoredEfficiency
     *
     * @param string $restoredEfficiency
     * @return Treatment
     */
    public function setRestoredEfficiency($restoredEfficiency)
    {
        $this->restoredEfficiency = $restoredEfficiency;

        return $this;
    }

    /**
     * Get restoredEfficiency
     *
     * @return string 
     */
    public function getRestoredEfficiency()
    {
        return $this->restoredEfficiency;
    }

    /**
     * Set oncologicalObserving
     *
     * @param \DateTime $oncologicalObserving
     * @return Treatment
     */
    public function setOncologicalObserving($oncologicalObserving)
    {
        $this->oncologicalObserving = $oncologicalObserving;

        return $this;
    }

    /**
     * Get oncologicalObserving
     *
     * @return \DateTime 
     */
    public function getOncologicalObserving()
    {
        return $this->oncologicalObserving;
    }

    /**
     * Set xrayObserving
     *
     * @param \DateTime $xrayObserving
     * @return Treatment
     */
    public function setXrayObserving($xrayObserving)
    {
        $this->xrayObserving = $xrayObserving;

        return $this;
    }

    /**
     * Get xrayObserving
     *
     * @return \DateTime 
     */
    public function getXrayObserving()
    {
        return $this->xrayObserving;
    }

    /**
     * Set contract
     *
     * @param \Medcard\Entity\Contract $contract
     * @return Treatment
     */
    public function setContract(\Medcard\Entity\Contract $contract = null)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * Get contract
     *
     * @return \Medcard\Entity\Contract 
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * Set medcard
     *
     * @param \Common\Entity\Medcard $medcard
     * @return Treatment
     */
    public function setMedcard(\Common\Entity\Medcard $medcard = null)
    {
        $this->medcard = $medcard;

        return $this;
    }

    /**
     * Get medcard
     *
     * @return \Common\Entity\Medcard 
     */
    public function getMedcard()
    {
        return $this->medcard;
    }

    /**
     * Add operations
     *
     * @param \Medcard\Entity\Operation $operations
     * @return Treatment
     */
    public function addOperation(\Medcard\Entity\Operation $operations)
    {
        $this->operations[] = $operations;

        return $this;
    }

    /**
     * Remove operations
     *
     * @param \Medcard\Entity\Operation $operations
     */
    public function removeOperation(\Medcard\Entity\Operation $operations)
    {
        $this->operations->removeElement($operations);
    }

    /**
     * Get operations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Add examinationResult
     *
     * @param \Medcard\Entity\ExaminationResult $examinationResult
     * @return Treatment
     */
    public function addExaminationResult(\Medcard\Entity\ExaminationResult $examinationResult)
    {
        $this->examinationResult[] = $examinationResult;

        return $this;
    }

    /**
     * Remove examinationResult
     *
     * @param \Medcard\Entity\ExaminationResult $examinationResult
     */
    public function removeExaminationResult(\Medcard\Entity\ExaminationResult $examinationResult)
    {
        $this->examinationResult->removeElement($examinationResult);
    }

    /**
     * Get examinationResult
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExaminationResult()
    {
        return $this->examinationResult;
    }

    /**
     * Add efficiencyMarks
     *
     * @param \Medcard\Entity\EfficiencyMarks $efficiencyMarks
     * @return Treatment
     */
    public function addEfficiencyMark(\Medcard\Entity\EfficiencyMarks $efficiencyMarks)
    {
        $this->efficiencyMarks[] = $efficiencyMarks;

        return $this;
    }

    /**
     * Remove efficiencyMarks
     *
     * @param \Medcard\Entity\EfficiencyMarks $efficiencyMarks
     */
    public function removeEfficiencyMark(\Medcard\Entity\EfficiencyMarks $efficiencyMarks)
    {
        $this->efficiencyMarks->removeElement($efficiencyMarks);
    }

    /**
     * Get efficiencyMarks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEfficiencyMarks()
    {
        return $this->efficiencyMarks;
    }

    /**
     * Add notebook
     *
     * @param \Medcard\Entity\Notebook $notebook
     * @return Treatment
     */
    public function addNotebook(\Medcard\Entity\Notebook $notebook)
    {
        $this->notebook[] = $notebook;

        return $this;
    }

    /**
     * Remove notebook
     *
     * @param \Medcard\Entity\Notebook $notebook
     */
    public function removeNotebook(\Medcard\Entity\Notebook $notebook)
    {
        $this->notebook->removeElement($notebook);
    }

    /**
     * Get notebook
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotebook()
    {
        return $this->notebook;
    }
    
    /**
     * Add ExpertInspection
     *
     * @param \Medcard\Entity\ExpertInspection $expertInspection
     * @return Treatment
     */
    public function addExpertInspection(\Medcard\Entity\ExpertInspection $expertInspection)
    {
        $this->expertInspection[] = $expertInspection;

        return $this;
    }

    /**
     * Remove ExpertInspection
     *
     * @param \Medcard\Entity\ExpertInspection $expertInspection
     */
    public function removeExpertInspection(\Medcard\Entity\ExpertInspection $expertInspection)
    {
        $this->expertInspection->removeElement($expertInspection);
    }

    /**
     * Get ExpertInspection
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExpertInspection()
    {
        return $this->expertInspection;
    }

    /**
     * Set treatmentDiagnosis
     *
     * @param \Medcard\Entity\TreatmentDiagnosis $treatmentDiagnosis
     * @return Treatment
     */
    public function setTreatmentDiagnosis(\Medcard\Entity\TreatmentDiagnosis $treatmentDiagnosis = null)
    {
        $this->treatmentDiagnosis = $treatmentDiagnosis;

        return $this;
    }

    /**
     * Get treatmentDiagnosis
     *
     * @return \Medcard\Entity\TreatmentDiagnosis 
     */
    public function getTreatmentDiagnosis()
    {
        return $this->treatmentDiagnosis;
    }

    /**
     * Set admissionDepartment
     *
     * @param \Medcard\Entity\AdmissionDepartment $admissionDepartment
     * @return Treatment
     */
    public function setAdmissionDepartment(\Medcard\Entity\AdmissionDepartment $admissionDepartment = null)
    {
        $this->admissionDepartment = $admissionDepartment;

        return $this;
    }

    /**
     * Get admissionDepartment
     *
     * @return \Medcard\Entity\AdmissionDepartment 
     */
    public function getAdmissionDepartment()
    {
        return $this->admissionDepartment;
    }
}
