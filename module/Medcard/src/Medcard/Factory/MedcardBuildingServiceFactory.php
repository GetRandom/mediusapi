<?php

namespace Medcard\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Medcard\Service\MedcardBuildingService;

class MedcardBuildingServiceFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $templatePath = 'data/template_medcard.docx';
        $service = new MedcardBuildingService();
        $service->setTemplatePath($templatePath);
        return $service;
    }

}
