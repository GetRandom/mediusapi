<?php

namespace Medcard\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Medcard\Controller\RegionController;

class RegionControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $controller = new RegionController();
        $controller->setEntityManager(
                $serviceLocator->getServiceLocator()->get('Doctrine\ORM\EntityManager')
        );
        return $controller;
    }

}
