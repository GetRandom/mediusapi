<?php

namespace Medcard\Service;

class MedcardBuildingService {

// PUBLIC:
    public function buildMedcard(\Common\Entity\Medcard $medcard) {
        $dateFormat = 'd-m-Y';
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $document = $phpWord->loadTemplate($this->templatePath);

        $hospitalization = $medcard->getHospitalization();
        $treatment = $medcard->getTreatment();
        $patient = $medcard->getPatient();
        
        $location = $patient->getLocation();
        
        $contract = $treatment->getContract();
        $operations = $treatment->getOperations();
        $treatmentDiagnosis = $treatment->getTreatmentDiagnosis();
        $efficiencyMarks = $treatment->getEfficiencyMarks();
        $admissionDepartment = $treatment->getAdmissionDepartment();
        $examinationResult = $treatment->getExaminationResult();
        $notebook = $treatment->getNotebook();
        $inspection = $treatment->getExpertInspection();

        // Medcard
        $document->setValue('number', $medcard->getNumber());
        $hospd = $medcard->getHospDate() ? $medcard->getHospDate()->format($dateFormat) : '';
        $document->setValue('hospd', $hospd);
        $document->setValue('separation', $medcard->getSeparation()->getTitle());
        $document->setValue('cham', $medcard->getChamber());
        //$document->setValue('cyh', $medcard->getCurrentYearHosp());
        $document->setValue('sender', $medcard->getSender());


        // Patient
        $document->setValue('p.name', $patient->getName());
        $document->setValue('p.lastname', $patient->getLastname());
        $document->setValue('p.patronymic', $patient->getPatronymic());
        $document->setValue('s', $patient->getSex());
        $born = $patient->getBorn() ? $patient->getBorn()->format($dateFormat) : '';
        $document->setValue('p.born', $born);
        $document->setValue('p.bt', $patient->getBloodType());
        $document->setValue('p.rh', $patient->getRhFactor());
        $rw = $patient->getRw() ? $patient->getRw()->format($dateFormat) : '';
        $document->setValue('p.rw', $rw);
        $hiv = $patient->getHivInfection() ? $patient->getHivInfection()->format($dateFormat) : '';
        $document->setValue('p.hiv', $hiv);
        $document->setValue('p.sensitivity', $patient->getSensitivity());
        $document->setValue('p.workplace', $patient->getWorkplace());
        $document->setValue('p.phone', $patient->getPhone());
        $document->setValue('p.paddr', 'Для приїжджих: ' . $patient->getParentsAddress());
        $document->setValue('p.age', $this->getAge($patient->getBorn()));
        
        // Location
        if ($location->getLocality()) {
            $document->setValue('l.reg', $location->getLocality()->getRegion()->getTitle());
            $document->setValue('l.area', $location->getArea());
            $document->setValue('l.loc', $location->getLocality()->getTitle());
        }
        $document->setValue('l.lt', $location->getLocalityType());
        $document->setValue('l.addr', $location->getAddress());

        // Hospitalization
        $document->setValue('h.ah', $hospitalization->getAfterHours());
        $document->setValue('h.ht', $hospitalization->getHospTerm());
        $document->setValue('h.idia', $hospitalization->getInstitutionDiagnosis());
        $document->setValue('h.hdia', $hospitalization->getHospitalizationDiagnosis());
        $document->setValue('h.cdia', $hospitalization->getClinicalDiagnosis());
        $setd = $hospitalization->getSettingDate() ? $hospitalization->getSettingDate()->format($dateFormat) : '';
        $document->setValue('h.setd', $setd);

        // Treatment
        $document->setValue('t.ot', $treatment->getOtherTreatment());
        $document->setValue('tt', $treatment->getOtherTreatmentTypes());
        $document->setValue('t.amount', $treatment->getAmount());
        $document->setValue('rs', $treatment->getResult());
        $document->setValue('t.re', $treatment->getRestoredEfficiency());
        $oo = $treatment->getOncologicalObserving() ? $treatment->getOncologicalObserving()->format($dateFormat) : '';
        $document->setValue('t.oo', $oo);
        $xo = $treatment->getXrayObserving() ? $treatment->getXrayObserving()->format($dateFormat) : '';
        $document->setValue('t.xo', $xo);
        
        // Contract
        $document->setValue('t.ins', $contract->getTitle() . ' ' . $contract->getDescription() . ' ' . $contract->getSubject());

        // Operations
        $maxLength = 3;
        for ($i = $operations->count(); $i < $maxLength; $i++) {
            $operations->add(new \Medcard\Entity\Operation());
        }
        $length = $operations->count();
        for ($i = 0; $i < $length; $i++) {
            $document->setValue('o.num_' . $i, $operations->get($i)->getNumber());
            $document->setValue('o.title_' . $i, $operations->get($i)->getTitle());
            $date = $operations->get($i)->getDate();
            $date = $date ? $date->format($dateFormat) : '';
            $document->setValue('o.date_' . $i, $date);
            $document->setValue('o.anm_' . $i, $operations->get($i)->getAnesthesiaMethod());
            $document->setValue('o.comp_' . $i, $operations->get($i)->getComplications());
            $document->setValue('o.surgeon_' . $i, $operations->get($i)->getSurgeon());
            $document->setValue('o.anest_' . $i, $operations->get($i)->getAnesthetist());
        }

        $document->setValue('d.final', $treatmentDiagnosis->getFinal());
        $document->setValue('d.main', $treatmentDiagnosis->getMain());
        $document->setValue('d.complication', $treatmentDiagnosis->getComplication());
        $document->setValue('d.related', $treatmentDiagnosis->getRelated());

        // EfficiencyMarks
        for ($i = 0; $i < $efficiencyMarks->count(); $i++) {
            $document->setValue('em.n_' . $i, $efficiencyMarks->get($i)->getNumber());
            $begin = $efficiencyMarks->get($i)->getBegin() ? $efficiencyMarks->get($i)->getBegin()->format($dateFormat) : '';
            $document->setValue('em.b_' . $i, $begin);
            $end = $efficiencyMarks->get($i)->getEnd() ? $efficiencyMarks->get($i)->getEnd()->format($dateFormat) : '';
            $document->setValue('em.e_' . $i, $end);
        }
        $maxEmarks = 4;
        for ($i = $efficiencyMarks->count(); $i < $maxEmarks; $i++) {
            $document->setValue('em.n_' . $i, '');
            $document->setValue('em.b_' . $i, '');
            $document->setValue('em.e_' . $i, '');
        }

        // AdmissionDepartment
        $document->setValue('ad.compl', $admissionDepartment->getComplaint());
        $document->setValue('ad.disa', $admissionDepartment->getDiseaseAnamnesis());
        $document->setValue('ad.life', $admissionDepartment->getLifeAnamnesis());
        $document->setValue('ad.obj', $admissionDepartment->getObjectiveState());
        $document->setValue('ad.other', $admissionDepartment->getOther());

        // ExaminationResult
        $len = $examinationResult->count();
        $document->cloneRow('er.date', $len);
        for ($i = 0; $i < $examinationResult->count(); $i++) {
            $exDate = $examinationResult->get($i)->getDate() ? $examinationResult->get($i)->getDate()->format($dateFormat) : '';
            $document->setValue('er.date#' . ($i + 1), $exDate);
            $document->setValue('er.note#' . ($i + 1), $examinationResult->get($i)->getNote());
        }

        // Notebook
        $notebookLen = $notebook->count();
        $document->cloneRow('n.date', $notebookLen);
        for ($i = 0; $i < $notebook->count(); $i++) {
            $noteDate = $notebook->get($i)->getDate() ? $notebook->get($i)->getDate()->format($dateFormat) : '';
            $document->setValue('n.date#' . ($i + 1), $noteDate);
            $document->setValue('n.note#' . ($i + 1), $notebook->get($i)->getNote());
        }
        
        // ExpertInspection
        $expLen = $inspection->count();
        $document->cloneRow('ex.date', $expLen);
        for ($i = 0; $i < $inspection->count(); $i++) {
            $noteDate = $inspection->get($i)->getDate() ? $inspection->get($i)->getDate()->format($dateFormat) : '';
            $document->setValue('ex.date#' . ($i + 1), $noteDate);
            $document->setValue('ex.note#' . ($i + 1), $inspection->get($i)->getNote());
            $document->setValue('ex.author#' . ($i + 1), $inspection->get($i)->getAuthor());
        }
        
        return $document;
    }

    /**
     * 
     * @param string $path
     */
    public function setTemplatePath($path) {
        $this->templatePath = $path;
    }

// PROTECTED:
    protected $templatePath;

// PRIVATE:
    /**
     * 
     * @param \DateTime $date
     * @return string
     */
    private function getAge(\DateTime $date) {
        $now = new \DateTime();
        $inter = $now->diff($date);
        return $inter->y;
    }

}
