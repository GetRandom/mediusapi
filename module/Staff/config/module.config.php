<?php

return array(
    'router' => array(
        'routes' => array(
            'staff_api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/:controller/:action[/:id]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Staff\Service\AuthService' => 'Staff\Factory\AuthServiceFactory',
            'Staff\Provider\Identity\StaffIdentityProvider' => 'Staff\Factory\StaffIdentityProviderFactory',
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'staff' => 'Staff\Factory\StaffControllerFactory',
            'digest' => 'Staff\Factory\DigestControllerFactory',
            'auth' => 'Staff\Factory\AuthControllerFactory'
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'staff_entity' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/Staff/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Staff\Entity' => 'staff_entity',
                )
            )
        )
    ),
    'bjyauthorize' => array(
        'default_role' => 'guest',
        'authenticated_role' => 'nurse',
        'identity_provider' => 'Staff\Provider\Identity\StaffIdentityProvider',
        'role_providers' => array(
            // using an object repository (entity repository) to load all roles into our ACL
            'BjyAuthorize\Provider\Role\ObjectRepositoryProvider' => array(
                'object_manager' => 'doctrine.entity_manager.orm_default',
                'role_entity_class' => 'Staff\Entity\Role',
            ),
        ),
        'guards' => array(
            /* If this guard is specified here (i.e. it is enabled), it will block
             * access to all controllers and actions unless they are specified here.
             * You may omit the 'action' index to allow access to the entire controller
             */
            'BjyAuthorize\Guard\Controller' => array(
                /* STAFF */
                array(
                    'controller' => 'staff',
                    'roles' => array('administrator')
                ),
                /* DIGEST */
                array(
                    'controller' => 'digest',
                    'roles' => array('administrator')
                ),
                /* AUTH */
                array(
                    'controller' => 'auth',
                    'roles' => array()
                ),
            ),
        ),
    ),
);
