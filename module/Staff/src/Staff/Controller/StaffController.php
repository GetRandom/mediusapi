<?php

namespace Staff\Controller;

use Zend\InputFilter\InputFilterInterface;
use Staff\Form\StaffInputFilter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Staff\Entity\Staff;

class StaffController extends AbstractActionController {

    private $entityManager;

    public function allAction() {
        $offset = $this->params()->fromQuery('offset', 0);
        $count = $this->params()->fromQuery('count', 0);
        $data = $this->entityManager->getRepository('\Staff\Entity\Staff')
                ->findAllAsArray($offset, $count);
        return new JsonModel(array('data' => $data));
    }

    public function addAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            $staff = new Staff();
            $filter = new StaffInputFilter();
            $filter->setData($data);
            $isValid = $filter->setValidationGroup(InputFilterInterface::VALIDATE_ALL)->isValid();
            if ($isValid = true) { // PLUG. Change validation
                $this->_FromArrayToStaff($data, $staff);
                $bcrypt = new \Zend\Crypt\Password\Bcrypt();
                $staff->setPassword($bcrypt->create($staff->getPassword()));
                $this->entityManager->persist($staff);
                $this->entityManager->flush();
            } else {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(array('data' => $filter->getMessages()));
            }
        }

        return new JsonModel();
    }

    public function updateAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->entityManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $data = $request->getPost()->toArray();
            $staff = $this->entityManager->getRepository('\Staff\Entity\Staff')->findOneBy(array('id' => $data['id']));
            $oldPassword = $staff->getPassword();
            $filter = new StaffInputFilter();
            $filter->setData($data);
            $isValid = $filter->setValidationGroup(InputFilterInterface::VALIDATE_ALL)->isValid();
            if ($isValid = true) { // PLUG. Change validation
                $this->_FromArrayToStaff($data, $staff);
                if ($staff->getPassword() !== '') {
                    $bcrypt = new \Zend\Crypt\Password\Bcrypt();
                    $staff->setPassword($bcrypt->create($staff->getPassword()));
                } else {
                    $staff->setPassword($oldPassword);
                }
                $this->entityManager->persist($staff);
                $this->entityManager->flush();
            } else {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(array('data' => $filter->getMessages()));
            }
        }

        return new JsonModel();
    }

    public function removeAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $id = (int) $request->getPost('id');
            $staff = $this->entityManager->getRepository('\Staff\Entity\Staff')
                    ->findOneBy(array('id' => $id));
            $this->entityManager->remove($staff);
            $this->entityManager->flush();
        }

        return new JsonModel();
    }

    public function viewAction() {
        $id = (int) $this->params()->fromRoute('id');
        $data = $this->entityManager->getRepository('\Staff\Entity\Staff')
                ->findOneByAsArray(array('id' => $id));
        $data['password'] = '';
        return new JsonModel(array('data' => $data));
    }

    public function passGenAction() {
        $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $max = 6;
        $size = StrLen($chars) - 1;
        $password = null;
        while ($max--) {
            $password.=$chars[rand(0, $size)];
        }
        return new JsonModel(['password' => $password]);
    }

    public function setEntityManager($entityManager) {
        $this->entityManager = $entityManager;
    }

    private function _FromArrayToStaff(array $data, $object) {
        $object->setLogin($data['login']);
        $object->setPassword($data['password']);
        $object->setName($data['name']);
        $object->setLastname($data['lastname']);
        $object->setPatronymic($data['patronymic']);
        $object->setPosts($this->entityManager->getRepository('Staff\Entity\Post')
                        ->SelectIn($data['posts']));
        $object->setSeparations($this->entityManager->getRepository('Staff\Entity\Separation')
                        ->SelectIn($data['separations']));
        $object->setWorkplaces($this->entityManager->getRepository('Staff\Entity\Workplace')
                        ->SelectIn($data['workplaces']));
        $object->setRoles($this->entityManager->getRepository('Staff\Entity\Role')
                        ->SelectIn($data['roles']));

        return $object;
    }

}
