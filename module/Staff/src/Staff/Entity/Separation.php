<?php

namespace Staff\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Separation
 *
 * @ORM\Table(name="separation")
 * @ORM\Entity(repositoryClass="Staff\Repository\SeparationRepository")
 */
class Separation {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     */
    private $title;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Staff", mappedBy="separation")
     */
    private $staff;

    /**
     * Constructor
     */
    public function __construct() {
        $this->staff = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getStaff() {
        return $this->staff;
    }

    public function setStaff($staff) {
        $this->staff = $staff;
    }

}
