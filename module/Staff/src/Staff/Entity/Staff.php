<?php

namespace Staff\Entity;

use Doctrine\ORM\Mapping as ORM;
use BjyAuthorize\Provider\Role\ProviderInterface;
use Doctrine\Common\Collections\ArrayCollection;

//use ZfcUser\Entity\UserInterface;

/**
 * Staff
 *
 * @ORM\Table(name="staff", uniqueConstraints={@ORM\UniqueConstraint(name="IDUser_UNIQUE", columns={"id"}), @ORM\UniqueConstraint(name="login_UNIQUE", columns={"login"})})
 * @ORM\Entity(repositoryClass="Staff\Repository\StaffRepository")
 */
class Staff implements ProviderInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="login", type="string", length=45, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=128, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=45, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="patronymic", type="string", length=45, nullable=true)
     */
    private $patronymic;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Post", inversedBy="staff")
     * @ORM\JoinTable(name="staff_post",
     *   joinColumns={
     *     @ORM\JoinColumn(name="staff_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="post_id", referencedColumnName="id")
     *   }
     * )
     */
    private $post;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="staff")
     * @ORM\JoinTable(name="staff_role",
     *   joinColumns={
     *     @ORM\JoinColumn(name="staff_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     *   }
     * )
     */
    private $role;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Separation", inversedBy="staff")
     * @ORM\JoinTable(name="staff_separation",
     *   joinColumns={
     *     @ORM\JoinColumn(name="staff_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="separation_id", referencedColumnName="id")
     *   }
     * )
     */
    private $separation;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Workplace", inversedBy="staff")
     * @ORM\JoinTable(name="staff_workplace",
     *   joinColumns={
     *     @ORM\JoinColumn(name="staff_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="workplace_id", referencedColumnName="id")
     *   }
     * )
     */
    private $workplace;

    /**
     * Constructor
     */
    public function __construct() {
        $this->post = new ArrayCollection();
        $this->role = new ArrayCollection();
        $this->separation = new ArrayCollection();
        $this->workplace = new ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getLogin() {
        return $this->login;
    }

    public function setLogin($login) {
        $this->login = $login;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    public function getPatronymic() {
        return $this->patronymic;
    }

    public function setPatronymic($patronymic) {
        $this->patronymic = $patronymic;
    }

    public function getPosts() {
        return $this->post;
    }

    public function setPosts($posts) {
        $this->post = $posts;
    }

    public function addPost($post) {
        $this->post[] = $post;
    }

    public function getRoles() {
        return $this->role;
    }

    public function setRoles($roles) {
        $this->role = $roles;
    }

    public function addRole($role) {
        $this->role[] = $role;
    }

    public function getSeparations() {
        return $this->separation;
    }

    public function setSeparations($separations) {
        $this->separation = $separations;
    }

    public function addSeparation($separation) {
        $this->separation[] = $separation;
    }

    public function getWorkplaces() {
        return $this->workplace;
    }

    public function setWorkplaces($workplaces) {
        $this->workplace = $workplaces;
    }

    public function addWorkplace($workplace) {
        $this->workplace[] = $workplace;
    }

}
