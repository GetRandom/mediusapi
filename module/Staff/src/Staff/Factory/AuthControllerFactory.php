<?php

namespace Staff\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Staff\Controller\AuthController;

class AuthControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $entityManager = $serviceLocator->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $authService = $serviceLocator->getServiceLocator()->get('Staff\Service\AuthService');
        $controller = new AuthController($entityManager, $authService);
        return $controller;
    }

}
