<?php

namespace Staff\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Staff\Controller\DigestController;

class DigestControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $controller = new DigestController();
        $controller->setEntityManager(
                $serviceLocator->getServiceLocator()->get('Doctrine\ORM\EntityManager')
        );
        return $controller;
    }

}
