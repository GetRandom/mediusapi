<?php

namespace Staff\Repository;

use Doctrine\ORM\EntityRepository;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class DigestEntityRepository extends EntityRepository {

    protected $hydrator;

    public function __construct($em, \Doctrine\ORM\Mapping\ClassMetadata $class) {
        parent::__construct($em, $class);

        $this->hydrator = new DoctrineHydrator($this->_em, $this->_entityName);
    }

    public function findAllAsArray() {
        $objects = $this->findAll();
        $data = array();
        for ($i = 0; $i < count($objects); $i++) {
            $data[$i] = $this->hydrator->extract($objects[$i]);
            unset($data[$i]['staff']);
        }
        return $data;
    }

    public function SelectIn(array $values) {
        $qb = $this->createQueryBuilder($this->_entityName);
        $qb->select('e')
                ->from($this->_entityName, 'e')
                ->where($qb->expr()->in('e.id', $values));
        $query = $qb->getQuery();

        return $query->getResult();
    }

}
