<?php

namespace Staff\Repository;

class RoleRepository extends DigestEntityRepository {

    public function findAllAsArray() {
        $objects = $this->findAll();
        $data = array();
        for ($i = 0; $i < count($objects); $i++) {
            $data[$i] = $this->hydrator->extract($objects[$i]);
            unset($data[$i]['staff']);
        }
        $result = array();
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i]['roleTitle'] != 'guest') {
                $result[] = $data[$i];
            }
        }
        return $result;
    }

}
