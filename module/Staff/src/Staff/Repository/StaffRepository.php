<?php

namespace Staff\Repository;

use Doctrine\ORM\EntityRepository;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class StaffRepository extends EntityRepository {

    protected $staffHydrator;
    protected $postHydrator;
    protected $workplaceHydrator;
    protected $separationHydrator;
    protected $roleHydrator;

    public function __construct($em, \Doctrine\ORM\Mapping\ClassMetadata $class) {
        parent::__construct($em, $class);

        $this->staffHydrator = new DoctrineHydrator($this->_em, '\Staff\Entity\Staff');
        $this->postHydrator = new DoctrineHydrator($this->_em, '\Staff\Entity\Post');
        $this->workplaceHydrator = new DoctrineHydrator($this->_em, '\Staff\Entity\Workplace');
        $this->separationHydrator = new DoctrineHydrator($this->_em, '\Staff\Entity\Separation');
        $this->roleHydrator = new DoctrineHydrator($this->_em, '\Staff\Entity\Role');
    }

    public function findAllAsArray($offset = 0, $count = 0) {
        $data = array();
        $query = $this->getEntityManager()->createQuery('select Staff from Staff\Entity\Staff as Staff ')
                        ->setFirstResult($offset)->setMaxResults($count);
        $objects = $query->getResult();
        for ($i = 0; $i < count($objects); $i++) {
            $data[$i] = $this->_extractStaff($objects[$i]);
        }
        return $data;
    }

    public function findOneByAsArray(array $criteria) {
        $staff = $this->findOneBy($criteria);
        return $this->_extractStaff($staff);
    }

    private function _extractStaff($staff) {
        $data = $this->staffHydrator->extract($staff);
        $data['posts'] = $this->_extractForEach($staff->getPosts(), $this->postHydrator);
        $data['workplaces'] = $this->_extractForEach($staff->getWorkplaces(), $this->workplaceHydrator);
        $data['separations'] = $this->_extractForEach($staff->getSeparations(), $this->separationHydrator);
        $data['roles'] = $this->_extractForEach($staff->getRoles(), $this->roleHydrator);
        return $data;
    }

    private function _extractForEach($collection, $hydrator) {
        $data = array();
        for ($i = 0; $i < count($collection); $i++) {
            $data[$i] = $hydrator->extract($collection[$i]);
            unset($data[$i]['staff']);
        }
        return $data;
    }

}
