<?php

namespace Staff\Service;

use Zend\Authentication\AuthenticationServiceInterface;
use Zend\Session\Container;

class AuthService implements AuthenticationServiceInterface {

    protected $entityManager;
    protected $identity;
    protected $login;
    protected $password;

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;

        $session = new Container('user');
        if (isset($session->id)) {
            $staff = $this->entityManager->getRepository('\Staff\Entity\Staff')->findOneBy(array('id' => $session->id));
            $this->setIdentity($staff);
        }
    }

    public function hasIdentity() {
        if ($this->identity instanceof \Staff\Entity\Staff) {
            return true;
        } else {
            return false;
        }
    }

    public function authenticate() {
        $session = new Container('user');
        $staff = $this->entityManager->getRepository('\Staff\Entity\Staff')->findOneBy(array('login' => $this->login));
        if ($staff) {
            $bcrypt = new \Zend\Crypt\Password\Bcrypt();
            if ($bcrypt->verify($this->password, $staff->getPassword())) {
                $this->setIdentity($staff);
                $session->id = $this->getIdentity()->getId();
                return true;
            }
        }
        $this->clearIdentity();
        return false;
    }

    public function getIdentity() {
        return $this->identity;
    }

    public function setIdentity($identity) {
        $this->identity = $identity;
    }

    public function setCredential($login, $password) {
        $this->login = $login;
        $this->password = $password;
    }

    public function clearIdentity() {
        $this->identity = null;
        $session = new Container('user');
        unset($session->id);
    }

}
