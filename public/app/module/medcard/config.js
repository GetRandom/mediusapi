/* factory */
angular.module('medicus')
        .factory('medcardApiFactory', medcardApiFactory, ['$http']);
angular.module('medicus')
        .factory('patientApiFactory', patientApiFactory, ['$http']);
angular.module('medicus')
        .factory('admissionDepartmentApiFactory', admissionDepartmentApiFactory, ['$http']);
angular.module('medicus')
        .factory('categoryApiFactory', categoryApiFactory, ['$http']);
angular.module('medicus')
        .factory('contractApiFactory', contractApiFactory, ['$http']);
angular.module('medicus')
        .factory('efficiencyMarkApiFactory', efficiencyMarkApiFactory, ['$http']);
angular.module('medicus')
        .factory('examinationResultApiFactory', examinationResultApiFactory, ['$http']);
angular.module('medicus')
        .factory('expertInspectionApiFactory', expertInspectionApiFactory, ['$http']);
angular.module('medicus')
        .factory('foodApiFactory', foodApiFactory, ['$http']);
angular.module('medicus')
        .factory('hospitalizationApiFactory', hospitalizationApiFactory, ['$http']);
angular.module('medicus')
        .factory('icdApiFactory', icdApiFactory, ['$http']);
angular.module('medicus')
        .factory('localityApiFactory', localityApiFactory, ['$http']);
angular.module('medicus')
        .factory('locationApiFactory', locationApiFactory, ['$http']);
angular.module('medicus')
        .factory('notebookApiFactory', notebookApiFactory, ['$http']);
angular.module('medicus')
        .factory('operationApiFactory', operationApiFactory, ['$http']);
angular.module('medicus')
        .factory('postApiFactory', postApiFactory, ['$http']);
angular.module('medicus')
        .factory('regionApiFactory', regionApiFactory, ['$http']);
angular.module('medicus')
        .factory('separationApiFactory', separationApiFactory, ['$http']);
angular.module('medicus')
        .factory('treatmentApiFactory', treatmentApiFactory, ['$http']);
angular.module('medicus')
        .factory('treatmentDiagnosisApiFactory', treatmentDiagnosisApiFactory, ['$http']);

/* controller */
angular.module('medicus')
        .controller('MedcardController', MedcardController, ['$scope', '$filter', 'medcardApiFactory']);
angular.module('medicus')
        .controller('PatientController', PatientController, ['$scope', 'patientApiFactory']);
angular.module('medicus')
        .controller('AdmissionDepartmentController', AdmissionDepartmentController, ['$scope', 'admissionDepartmentApiFactory']);
angular.module('medicus')
        .controller('CategoryController', CategoryController, ['$scope', 'categoryApiFactory']);
angular.module('medicus')
        .controller('ContractController', ContractController, ['$scope', 'contractApiFactory']);
angular.module('medicus')
        .controller('EfficiencyMarkController', EfficiencyMarkController, ['$scope', 'efficiencyMarkApiFactory']);
angular.module('medicus')
        .controller('ExaminationResultController', ExaminationResultController, ['$scope', 'examinationResultApiFactory']);
angular.module('medicus')
        .controller('ExpertInspectionController', ExpertInspectionController, ['$scope', 'expertInspectionApiFactory']);
angular.module('medicus')
        .controller('FoodController', FoodController, ['$scope', 'foodApiFactory']);
angular.module('medicus')
        .controller('HospitalizationController', HospitalizationController, ['$scope', 'hospitalizationApiFactory']);
angular.module('medicus')
        .controller('IcdController', IcdController, ['$scope', 'icdApiFactory']);
angular.module('medicus')
        .controller('LocationController', LocationController, ['$scope', 'locationApiFactory', 'localityApiFactory', 'regionApiFactory']);
angular.module('medicus')
        .controller('NotebookController', NotebookController, ['$scope', 'notebookApiFactory']);
angular.module('medicus')
        .controller('OperationController', OperationController, ['$scope', 'operationApiFactory']);
angular.module('medicus')
        .controller('PostController', PostController, ['$scope', 'postApiFactory']);
angular.module('medicus')
        .controller('SeparationController', SeparationController, ['$scope', 'separationApiFactory']);
angular.module('medicus')
        .controller('TreatmentController', TreatmentController, ['$rootScope', '$scope', 'treatmentApiFactory']);
angular.module('medicus')
        .controller('TreatmentDiagnosisController', TreatmentDiagnosisController, ['$scope', 'treatmentDiagnosisApiFactory']);