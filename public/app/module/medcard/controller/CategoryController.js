function CategoryController($scope, categoryApiFactory) {

    var categoryButtonsTemplate = '<div class="categoryBT"><button class="btn-override btn-success" ng-click="updateCategory(row.entity)"><i class="fa fa-check"> Оновити</i></button>'
            + '<button class="btn-override btn-danger" ng-click="removeCategory(row.entity)"><i class="fa fa-remove"> Видалити</i></button></div>';
    $scope.categoryGrid = {
        data: 'categories',
        enableCellSelection: true,
        enableCellEdit: true,
        enableRowSelection: false,
        columnDefs: [
            {field: 'title', displayName: 'Назва', headerClass: 'text-center', enableCellEdit: true},
            {cellClass: 'text-center', cellTemplate: categoryButtonsTemplate}
        ]
    };
    $scope.newCategory = {title: null};

// HANDLERS:
    var allCategoriesObtained = function (data) {
        $scope.categories = data;
        // For medcard/index
        if ($scope.medcards) {
            for (var i = 0; i < $scope.medcards.length; i++) {
                for (var j = 0; j < data.length; j++) {
                    if ($scope.medcards[i].patient) {
                        if ($scope.medcards[i].patient.category === data[j].id) {
                            $scope.medcards[i].patient.category = data[j];
                        }
                    }
                }
            }
            linkOnRow();
        }
        console.log('allCategoriesObtained', data);
    };
    var categoryAdded = function (data) {
        console.log('categoryAdded', data);
        $scope.allCategories();
        $scope.newCategory = null;
    };
    var categoryUpdated = function (data) {
        console.log('categoryUpdated', data);
        $scope.allCategories();
    };
    var categoryRemoved = function (data) {
        console.log('categoryRemoved', data);
        $scope.allCategories();
    };
    var error = function (data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.$on('allPatientsObtained', function (event, data) {
        $scope.allCategories();
    });

    $scope.allCategories = function () {
        var request = categoryApiFactory.all();
        request.success(allCategoriesObtained);
        request.error(error);
    };
    $scope.addCategory = function (category) {
        var request = categoryApiFactory.add(category);
        request.success(categoryAdded);
        request.error(error);
    };
    $scope.updateCategory = function (category) {
        var request = categoryApiFactory.update(category);
        request.success(categoryUpdated);
        request.error(error);
    };
    $scope.removeCategory = function (category) {
        var request = categoryApiFactory.remove(category);
        request.success(categoryRemoved);
        request.error(error);
    };
}
;