function ContractController($scope, contractApiFactory) {

    var contractButtonsTemplate = '<div class="categoryBT"><button class="btn-override btn-success" ng-click="updateContract(row.entity)"><i class="fa fa-check"> Оновити</i></button>'
            + '<button class="btn-override btn-danger" ng-click="removeContract(row.entity)"><i class="fa fa-remove"> Видалити</i></button></div>';
    $scope.contractGrid = {
        data: 'contracts',
        enableCellSelection: true,
        enableCellEdit: true,
        enableRowSelection: false,
        columnDefs: [
            {field: 'title', displayName: 'Найменування', headerClass: 'text-center', enableCellEdit: true},
            {field: 'description', displayName: '№  договору (термін дії)(дод.угоди)', headerClass: 'text-center', enableCellEdit: true},
            {field: 'subject', displayName: 'Предмет договору', headerClass: 'text-center', enableCellEdit: true},
            {cellClass: 'text-center', cellTemplate: contractButtonsTemplate}
        ]
    };
    $scope.newContract;

// HANDLERS:
    var allContractsObtained = function (data) {
        $scope.contracts = data;
        console.log('allContractsObtained', data);
    };
    var contractAdded = function (data) {
        console.log('contractAdded', data);
        $scope.allContracts();
        $scope.newContract = null;
    };
    var contractUpdated = function (data) {
        console.log('contractUpdated', data);
        $scope.allContracts();
    };
    var contractRemoved = function (data) {
        console.log('contractRemoved', data);
        $scope.allContracts();
    };
    var error = function (data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.allContracts = function () {
        var request = contractApiFactory.all();
        request.success(allContractsObtained);
        request.error(error);
    };
    $scope.addContract = function (data) {
        var request = contractApiFactory.add(data);
        request.success(contractAdded);
        request.error(error);
    };
    $scope.updateContract = function (data) {
        var request = contractApiFactory.update(data);
        request.success(contractUpdated);
        request.error(error);
    };
    $scope.removeContract = function (category) {
        var request = contractApiFactory.remove(category);
        request.success(contractRemoved);
        request.error(error);
    };
}
;