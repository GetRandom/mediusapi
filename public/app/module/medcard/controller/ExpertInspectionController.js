function ExpertInspectionController($scope, expertInspectionApiFactory) {
    $scope.inspections = {};

    var editCellTemp = '<div class="pdn-5"><textarea rows="3" class="form-control" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" ></textarea></div>';
    var cellTemp = '<div ng-class="col.colIndex()"><div class="pdn-5" ng-cell-text>{{row.getProperty(col.field)}}</div></div>';

    $scope.inspectionsGrid = {
        data: 'inspections',
        enableCellSelection: true,
        enableCellEdit: true,
        enableRowSelection: false,
        rowHeight: 90,
        columnDefs: [
            {field: 'date.date', displayName: 'Дата', width: 145, headerClass: 'text-center',
                cellTemplate: '<div class="pdn-5"><input class="form-control-treat" type="date" ng-model="row.entity.date.date"></div>'},
            {field: 'note', displayName: 'Нотатки', headerClass: 'text-center',
                editableCellTemplate: editCellTemp,
                cellTemplate: cellTemp},
            {field: 'author', displayName: 'Спеціаліст', headerClass: 'text-center',
                editableCellTemplate: editCellTemp,
                cellTemplate: cellTemp},
            {width:75, cellTemplate: '<div class="text-center pdn-5"><button title="Зберегти" class="btn-treat btn-success" ng-click="updateExpertInspection(row.entity)"><i class="fa fa-save"></i></button>\n\
                                <button title="Стерти" class="btn-treat btn-danger" ng-click="removeExpertInspection(row.entity)"><i class="fa fa-eraser"></i></button></div>'}
        ]
    };

// HANDLERS:
    var allExpertInspectionsObtained = function (data) {
        for (var i = 0; i < data.length; i++) {
            data[i].date = phpDateToJs(data[i].date);
        }
        data.push({note: null, date: null, author: null});
        $scope.inspections = data;
        console.log('allExpertInspectionsObtained', data);
    };
    var expertInspectionUpdated = function (data) {
        console.log('expertInspectionUpdated', data);
        $scope.allExpertInspections($scope.medcard.treatment);
    };
    var expertInspectionRemoved = function (data) {
        console.log('expertInspectionRemoved', data);
        $scope.allExpertInspections($scope.medcard.treatment);
    };

// ACTIONS:
    $scope.$on('treatmentObtained', function (event, data) {
        $scope.allExpertInspections(data);
    });

    $scope.allExpertInspections = function (data) {
        var request = expertInspectionApiFactory.all(data.id);
        request.success(allExpertInspectionsObtained);
    };
    $scope.updateExpertInspection = function (data) {
        var note = {};
        angular.copy(data, note);
        note.date = jsDateToYmd(note.date);
        note.treatment = $scope.medcard.treatment.id;
        var request = expertInspectionApiFactory.update(note);
        request.success(expertInspectionUpdated);
    };

    $scope.removeExpertInspection = function (data) {
        var request = expertInspectionApiFactory.remove(data);
        request.success(expertInspectionRemoved);
    };

}
;