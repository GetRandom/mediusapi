function FoodController($scope, foodApiFactory) {

    var template = '<div class="categoryBT"><button class="btn-override btn-success" ng-click="updateFood(row.entity)"><i class="fa fa-check"> Оновити</i></button>'
            + '<button class="btn-override btn-danger" ng-click="removeFood(row.entity)"><i class="fa fa-remove"> Видалити</i></button></div>';
    $scope.foodGrid = {
        data: 'food',
        enableCellSelection: true,
        enableCellEdit: true,
        enableRowSelection: false,
        columnDefs: [
            {field: 'title', displayName: 'Назва', headerClass: 'text-center', enableCellEdit: true},
            {cellClass: 'text-center', cellTemplate: template}
        ]
    };
    $scope.newFood = {title: null};

// HANDLERS:
    var allFoodObtained = function (data) {
        $scope.food = data;
        console.log('allFoodObtained', data);
    };
    var foodAdded = function (data) {
        console.log('foodAdded', data);
        $scope.allFood();
        $scope.newFood = null;
    };
    var foodUpdated = function (data) {
        console.log('foodUpdated', data);
        $scope.allFood();
    };
    var foodRemoved = function (data) {
        console.log('foodRemoved', data);
        $scope.allFood();
    };
    var error = function (data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.allFood = function () {
        var request = foodApiFactory.all();
        request.success(allFoodObtained);
        request.error(error);
    };
    $scope.addFood = function (data) {
        var request = foodApiFactory.add(data);
        request.success(foodAdded);
        request.error(error);
    };
    $scope.updateFood = function (data) {
        var request = foodApiFactory.update(data);
        request.success(foodUpdated);
        request.error(error);
    };
    $scope.removeFood = function (data) {
        var request = foodApiFactory.remove(data);
        request.success(foodRemoved);
        request.error(error);
    };
}
;