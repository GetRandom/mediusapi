function HospitalizationController($scope, hospitalizationApiFactory) {

// HANDLERS:
    var hospitalizationObtained = function (data) {
        data.settingDate = phpDateToJs(data.settingDate);
        $scope.medcard.hospitalization = data;
        console.log('hospitalizationObtained', data);
    };
    var hospitalizationUpdated = function (data) {
        console.log('hospitalizationUpdated', data);
    };

    var error = function (data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.$on('medcardObtained', function (event, data) {
        $scope.getHospitalization(data);
    });
    $scope.$on('medcardUpdating', function (event, data) {
        $scope.updateHospitalization(data.hospitalization);
    });

    $scope.getHospitalization = function (data) {
        var request = hospitalizationApiFactory.view(data.id);
        request.success(hospitalizationObtained);
        request.error(error);
    };
    $scope.updateHospitalization = function (data) {
        var hospitalization = {};
        angular.copy(data, hospitalization);
        hospitalization.settingDate = jsDateToYmd(hospitalization.settingDate);
        var request = hospitalizationApiFactory.update(hospitalization);
        request.success(hospitalizationUpdated);
        request.error(error);
    };

}
;