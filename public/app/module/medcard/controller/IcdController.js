function IcdController($scope, icdApiFactory) {

    $scope.currentClass = {};
    $scope.currentClass.id = 1;
    $scope.currentBlock = {};

    $scope.allClasses = function () {
        var icdRequest = icdApiFactory.all();
        var icdSuccess = function (data) {
            $scope.classes = data;
        };
        icdRequest.success(icdSuccess);
    };

    $scope.allBlocks = function () {
        var icdRequest = icdApiFactory.allBlocks();
        var icdSuccess = function (data) {
            $scope.blocks = data;
        };
        icdRequest.success(icdSuccess);
    };
    
    $scope.allDiagnosis = function () {
        var icdRequest = icdApiFactory.allDiagnosis();
        var icdSuccess = function (data) {
            $scope.diagnosis = data;
            console.log('allDiagnosisObtained', data);
        };
        icdRequest.success(icdSuccess);
    };

    $scope.getBlocks = function (icdClass) {
        var icdRequest = icdApiFactory.class(icdClass.id);
        var icdSuccess = function (data) {
            icdClass.blocks = data;
        };
        icdRequest.success(icdSuccess);
    };
}
;