
function MedcardController($scope, $filter, medcardApiFactory) {
    $scope.medcards = {};
    $scope.medcard = {};
    $scope.medcard.number = null;
    $scope.currentYear = (new Date()).getFullYear() + '-';

// HANDLERS:
    var allMedcardsObtained = function (data) {
        $scope.medcards = data;
        $scope.$broadcast('allMedcardsObtained', data);
        console.log('allMedcardsObtained', data);
    };
    var medcardObtained = function (data) {
        data.hospDate = phpDateToJs(data.hospDate);
        data.subNumber = data.number.substr(5, data.number.length);
        $scope.medcard = data;
        $scope.$broadcast('medcardObtained', data);
        console.log('medcardObtained', data);
    };
    var medcardAdded = function (data) {
        window.location = '/medcard/view/' + $scope.medcard.number;
        console.log('medcardAdded', data);
    };
    var medcardUpdated = function (data) {
        console.log('medcardUpdated', data);
    };
    var medcardRemoved = function (data) {
        window.location = '/medcard';
        console.log('medcardRemoved', data);
    };

    var error = function (data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.allMedcards = function () {
        var request = medcardApiFactory.all();
        request.success(allMedcardsObtained);
        request.error(error);
    };
    $scope.getMedcard = function (id) {
        var request = medcardApiFactory.view(id);
        request.success(medcardObtained);
        request.error(error);
    };
    $scope.addMedcard = function (data) {
        data.number = $scope.currentYear + data.subNumber;
        var request = medcardApiFactory.add(data);
        request.success(medcardAdded);
        request.error(error);
    };
    $scope.updateMedcard = function (data) {
        var medcard = {};
        angular.copy(data, medcard);
        $scope.$broadcast('medcardUpdating', medcard);
        medcard.hospDate = jsDateToYmd(medcard.hospDate);
        medcard.number = medcard.number.substr(0, 5) + medcard.subNumber;
        var request = medcardApiFactory.update(medcard);
        request.success(medcardUpdated);
        request.error(error);
    };
    $scope.removeMedcard = function (data) {
        var request = medcardApiFactory.remove(data);
        request.success(medcardRemoved);
        request.error(error);
    };
    $scope.downloadMedcard = function (data) {
        window.location = '/api/medcard-download/download/' + data.id;
    };
    $scope.archiveMedcard = function (data) {
        alert('Not implemented in current version.');
    };

// COMMON:
    $scope.getIdFromUrl = function () {
        var str = window.location.href;
        return str.substring(str.lastIndexOf('/') + 1);
    };


    /////////////////////////////////////////////////////////////////////
    $scope.currentPage = 0;
    $scope.pageSize = 10;

    $scope.numberOfPages = function () {
        var myFilteredData = $filter('filter')($scope.medcards, $scope.search_notes);
        return Math.ceil(myFilteredData.length / $scope.pageSize);
    };
}
;