function NotebookController($scope, notebookApiFactory) {
    $scope.notebooks = {};

    var editCellTemp = '<div class="pdn-5"><textarea rows="3" class="form-control" ng-class="\'colt\' + col.index" ng-input="COL_FIELD" ng-model="COL_FIELD" ></textarea></div>';
    var cellTemp = '<div ng-class="col.colIndex()"><div class="pdn-5" ng-cell-text>{{row.getProperty(col.field)}}</div></div>';

    $scope.notebooksGrid = {
        data: 'notebooks',
        enableCellSelection: true,
        enableCellEdit: true,
        enableRowSelection: false,
        rowHeight: 90,
        columnDefs: [
            {field: 'begin.date', displayName: 'Дата', width: 145, headerClass: 'text-center',
                cellTemplate: '<div class="pdn-5"><input class="form-control-treat" type="date" ng-model="row.entity.date.date"></div>'},
            {field: 'note', displayName: 'Нотатки', headerClass: 'text-center',
                editableCellTemplate: editCellTemp,
                cellTemplate: cellTemp},
            {width:75, cellTemplate: '<div class="text-center pdn-5"><button title="Зберегти" class="btn-treat btn-success" ng-click="updateNotebook(row.entity)"><i class="fa fa-save"></i></button>\n\
                                <button title="Стерти" class="btn-treat btn-danger" ng-click="removeNotebook(row.entity)"><i class="fa fa-eraser"></i></button></div>'}
        ]
    };

// HANDLERS:
    var allNotebooksObtained = function (data) {
        for (var i = 0; i < data.length; i++) {
            data[i].date = phpDateToJs(data[i].date);
        }
        data.push({title: null, date: null});
        $scope.notebooks = data;
        console.log('allNotebooksObtained', data);
    };
    var notebookUpdated = function (data) {
        console.log('notebookUpdated', data);
        $scope.allNotebooks($scope.medcard.treatment);
    };
    var notebookRemoved = function (data) {
        console.log('notebookRemoved', data);
        $scope.allNotebooks($scope.medcard.treatment);
    };

// ACTIONS:
    $scope.$on('treatmentObtained', function (event, data) {
        $scope.allNotebooks(data);
    });

    $scope.allNotebooks = function (data) {
        var request = notebookApiFactory.all(data.id);
        request.success(allNotebooksObtained);
    };
    $scope.updateNotebook = function (data) {
        var note = {};
        angular.copy(data, note);
        note.date = jsDateToYmd(note.date);
        note.treatment = $scope.medcard.treatment.id;
        var request = notebookApiFactory.update(note);
        request.success(notebookUpdated);
    };

    $scope.removeNotebook = function (data) {
        var request = notebookApiFactory.remove(data);
        request.success(notebookRemoved);
    };

}
;