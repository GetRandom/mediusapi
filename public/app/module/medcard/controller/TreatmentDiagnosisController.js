function TreatmentDiagnosisController($scope, treatmentDiagnosisApiFactory) {

// HANDLERS:
    var treatmentDiagnosisObtained = function (data) {
        $scope.medcard.treatment.treatmentDiagnosis = data;
        console.log('treatmentDiagnosisObtained', data);
    };
    var treatmentDiagnosisUpdated = function (data) {
        console.log('treatmentDiagnosisUpdated', data);
    };

    var error = function (data) {
        console.log('error', data);
    };

// ACTIONS:
    $scope.$on('treatmentObtained', function (event, data) {
        $scope.getTreatmentDiagnosis(data);
    });
    $scope.$on('treatmentUpdating', function (event, data) {
        $scope.updateTreatmentDiagnosis(data.treatmentDiagnosis);
    });

    $scope.getTreatmentDiagnosis = function (data) {
        var request = treatmentDiagnosisApiFactory.view(data.id);
        request.success(treatmentDiagnosisObtained);
        request.error(error);
    };
    $scope.updateTreatmentDiagnosis = function (data) {
        var request = treatmentDiagnosisApiFactory.update(data);
        request.success(treatmentDiagnosisUpdated);
        request.error(error);
    };
}
;