function categoryApiFactory($http) {
    var factory = {};

    factory.all = function () {
        return $http.get('/api/category/all');
    };

    factory.view = function (id) {
        return $http.get('/api/category/view/' + id);
    };

    factory.add = function (category) {
        return  $http({
            method: 'POST',
            url: '/api/category/add',
            data: $.param(category),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    };

    factory.update = function (category) {
        return  $http({
            method: 'POST',
            url: '/api/category/update',
            data: $.param(category),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    };

    factory.remove = function (category) {
        return  $http({
            method: 'POST',
            url: '/api/category/remove',
            data: $.param(category),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    };

    return factory;
}
;