function efficiencyMarkApiFactory($http) {
    var factory = {};
    factory.all = function (id) {
        return $http.get('/api/efficiency-mark/all/' + id);
    };
    factory.update = function (data) {
        return $http.post('/api/efficiency-mark/update', data);
    };
    factory.remove = function (data) {
        return $http.post('/api/efficiency-mark/remove', data);
    };
    return factory;
}
;