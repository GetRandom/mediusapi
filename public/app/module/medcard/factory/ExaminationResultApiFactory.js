function examinationResultApiFactory($http) {
    var factory = {};
    factory.all = function (id) {
        return $http.get('/api/examination-result/all/' + id);
    };
    factory.update = function (data) {
        return $http.post('/api/examination-result/update', data);
    };
    factory.remove = function (data) {
        return $http.post('/api/examination-result/remove', data);
    };
    return factory;
}
;