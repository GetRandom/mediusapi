function expertInspectionApiFactory($http) {
    var factory = {};
    factory.all = function (id) {
        return $http.get('/api/expert-inspection/all/' + id);
    };
    factory.update = function (data) {
        return $http.post('/api/expert-inspection/update', data);
    };
    factory.remove = function (data) {
        return $http.post('/api/expert-inspection/remove', data);
    };
    return factory;
}
;