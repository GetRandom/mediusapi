function locationApiFactory($http) {
    var factory = {};

    factory.view = function (id) {
        return $http.get('/api/location/view/' + id);
    };

    factory.update = function (location) {
        return  $http({
            method: 'POST',
            url: '/api/location/update',
            data: $.param(location),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    };

    return factory;
}
;