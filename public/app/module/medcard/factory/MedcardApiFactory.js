function medcardApiFactory($http) {
    var factory = {};
    factory.all = function () {
        return $http.get('/api/medcard/all');
    };
    factory.view = function (id) {
        return $http.get('/api/medcard/view/' + id);
    };
    factory.add = function (medcard) {
        return $http.post('/api/medcard/add', medcard);
    };
    factory.update = function (medcard) {
        return $http.post('/api/medcard/update', medcard);
    };
    factory.remove = function (medcard) {
        return $http.post('/api/medcard/remove', medcard);
    };
    return factory;
}
;