function notebookApiFactory($http) {
    var factory = {};
    factory.all = function (id) {
        return $http.get('/api/notebook/all/' + id);
    };
    factory.update = function (data) {
        return $http.post('/api/notebook/update', data);
    };
    factory.remove = function (data) {
        return $http.post('/api/notebook/remove', data);
    };
    return factory;
}
;