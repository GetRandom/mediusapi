function operationApiFactory($http) {
    var factory = {};
    factory.all = function (id) {
        return $http.get('/api/operation/all/' + id);
    };
    factory.update = function (operation) {
        return $http.post('/api/operation/update', operation);
    };
    factory.remove = function (operation) {
        return $http.post('/api/operation/remove', operation);
    };
    return factory;
}
;