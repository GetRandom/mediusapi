function patientApiFactory($http) {
    var factory = {};
    factory.all = function () {
        return $http.get('/api/patient/all');
    };
    factory.view = function (id) {
        return $http.get('/api/patient/view/' + id);
    };
    factory.update = function (patient) {
        return $http.post('/api/patient/update', patient);
    };
    return factory;
}
;