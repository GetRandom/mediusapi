function separationApiFactory($http) {
    var factory = {};

    factory.all = function () {
        return $http.get('/api/digest/get-separations');
    };

    factory.add = function (separation) {
        return  $http({
            method: 'POST',
            url: '/api/digest/add-separation',
            data: $.param(separation),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    };

    factory.update = function (separation) {
        return  $http({
            method: 'POST',
            url: '/api/digest/update-separation',
            data: $.param(separation),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    };

    factory.remove = function (separation) {
        return  $http({
            method: 'POST',
            url: '/api/digest/remove-separation',
            data: $.param(separation),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    };

    return factory;
}
;