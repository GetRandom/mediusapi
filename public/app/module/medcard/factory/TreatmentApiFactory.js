function treatmentApiFactory($http) {
    var factory = {};

    factory.view = function (id) {
        return $http.get('/api/treatment/view/' + id);
    };

    factory.update = function (treatment) {
        return $http.post('/api/treatment/update', treatment);
    };

    return factory;
}
;