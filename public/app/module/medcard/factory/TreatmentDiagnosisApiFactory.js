function treatmentDiagnosisApiFactory($http) {
    var factory = {};

    factory.view = function (id) {
        return $http.get('/api/treatment-diagnosis/view/' + id);
    };

    factory.update = function (data) {
        return $http.post('/api/treatment-diagnosis/update', data);
    };

    return factory;
}
;