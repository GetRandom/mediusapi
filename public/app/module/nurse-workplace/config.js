/* factory */
angular.module('ObservationList')
        .factory('medcardApiFactory', medcardApiFactory, ['$http']);
angular.module('ObservationList')
        .factory('patientApiFactory', patientApiFactory, ['$http']);
angular.module('ObservationList')
        .factory('ObservationApiFactory', ObservationApiFactory, ['$http']);

/* service */
angular.module('ObservationList')
        .service('RetrieveDataService', RetrieveDataService, []);
angular.module('ObservationList')
        .service('RenderChartsService', RenderChartsService, []);

/* controller */
angular.module('ObservationList')
        .controller('MedcardController', MedcardController, ['$scope', '$filter', 'medcardApiFactory']);
angular.module('ObservationList')
        .controller('PatientController', PatientController, ['$scope', 'patientApiFactory']);
angular.module('ObservationList')
        .controller('MainController', MainController, ['$scope', '$http', '$window', 'renderCharts', 'ObservationApiFactory', 'RetrieveData']);
angular.module('ObservationList')
        .controller('ObservationController', ObservationController, ['$scope', 'ObservationApiFactory']);