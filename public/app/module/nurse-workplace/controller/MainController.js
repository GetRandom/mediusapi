function MainController($scope, $http, $window, RenderChartsService, ObservationApiFactory, RetrieveDataService) {
    $scope.info = true;
    $scope.showingAddPatient = false;
    $scope.showPanels = true;
    $scope.showReplyForm = true;
    //var iduser = document.getElementById('iduser').value;
    $scope.reload = function () {
        $window.location.reload();
    };
    $scope.showingPanels = function () {
        $scope.info = false;
        $scope.showPanels = !$scope.showPanels;
    };
    $scope.showingReplyPatientData = function (patient) {
        $scope.replyPatient = {};
        $scope.showReplyForm = false;
        $scope.replyPatient.name = patient.name;
        $scope.replyPatient.id = patient.idpatients;
        $scope.replyPatient.card = patient.card;
    };
    $scope.addActive = function (index) {
        $scope.selected = index;
    };
    $scope.hideFormReply = function () {
        $scope.showReplyForm = true;
    };
    $scope.showAddForm = function () {
        $scope.showingAddPatient = !$scope.showingAddPatient;
    };
    $scope.cancelPatientForm = function () {
        $scope.showingAddPatient = false;
    };

    $scope.$on('observationChanged', function(event, medcard){
        $scope.selectedPatient(medcard);
    });

    // Changed for integration
    $scope.selectedPatient = function (medcard) {
        $scope.info = false;
        $scope.showPanels = true;
        $scope.selectedMedcard = medcard;
        $scope.idMedcard = medcard.id;
        $scope.namePatient = medcard.patient.name;
        ObservationApiFactory.getTemperatureById(medcard.id)
                .then(function (result) {
                    $scope.temperatures = result.data;
                    var obj = RetrieveDataService.retrieveTemperature(result.data);
                    var fullName = medcard.patient.lastname + ' ' + medcard.patient.name + ' ' + medcard.patient.patronymic;
                    RenderChartsService.renderTempChart(fullName, obj.dates, obj.temperature);
                    $scope.tempdates = obj.dates;
                    $scope.tempvalues = obj.temperature;
                });
        ObservationApiFactory.getPressureById(medcard.id)
                .then(function (result) {
                    $scope.pressures = result.data;
                    var obj = RetrieveDataService.retrievePressure(result.data);
                    var fullName = medcard.patient.lastname + ' ' + medcard.patient.name + ' ' + medcard.patient.patronymic;
                    RenderChartsService.renderPressureChart(fullName, obj.dates, obj.systolic, obj.diastolic);
                    $scope.pressuredates = obj.dates;
                    $scope.systolic = obj.systolic;
                    $scope.diastolic = obj.diastolic;
                });
        ObservationApiFactory.getPulseById(medcard.id)
                .then(function (result) {
                    $scope.pulseList = result.data;
                    var obj = RetrieveDataService.retrievePulse(result.data);
                    var fullName = medcard.patient.lastname + ' ' + medcard.patient.name + ' ' + medcard.patient.patronymic;
                    RenderChartsService.renderPulseChart(fullName, obj.dates, obj.pulse);
                    $scope.pulsedates = obj.dates;
                    $scope.pulses = obj.pulse;
                });
    };
}
;