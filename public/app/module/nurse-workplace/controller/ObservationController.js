function ObservationController($scope, ObservationApiFactory) {
    $scope.showTempForm = false;
    $scope.showPulseForm = false;
    $scope.showPressureForm = false;
    $scope.showDeleteTemp = false;
    $scope.showDeletePulse = false;
    $scope.showDeletePressure = false;
    $scope.toggleTemperatureForm = function () {
        $scope.showTempForm = !$scope.showTempForm;
    };
    $scope.togglePulseForm = function () {
        $scope.showPulseForm = !$scope.showPulseForm;
    };
    $scope.togglePressureForm = function () {
        $scope.showPressureForm = !$scope.showPressureForm;
    };
    $scope.toggleDeleteTemp = function () {
        $scope.showDeleteTemp = !$scope.showDeleteTemp;
    };
    $scope.toggleDeletePulse = function () {
        $scope.showDeletePulse = !$scope.showDeletePulse;
    };
    $scope.toggleDeletePressure = function () {
        $scope.showDeletePressure = !$scope.showDeletePressure;
    };

    $scope.addTemp = function (selectedMedcard, date, value) {
        ObservationApiFactory.addTemperatureById(selectedMedcard.id, {value: value, date: date})
                .success(function (data) {
                    $scope.$emit('observationChanged', selectedMedcard);
                    $scope.toggleTemperatureForm();
                });
    };

    $scope.deleteTemp = function (id, selectedMedcard) {
        ObservationApiFactory.deleteTemperatureById(id)
                .success(function (data) {
                    $scope.$emit('observationChanged', selectedMedcard);
                    $scope.toggleDeleteTemp();
                });
    };

    $scope.addPulse = function (selectedMedcard, date, pulse) {
        ObservationApiFactory.addPulseById(selectedMedcard.id, {date: date, value: pulse})
                .success(function (data) {
                    $scope.$emit('observationChanged', selectedMedcard);
                    $scope.togglePulseForm();
                });
    };

    $scope.deletePulse = function (id, selectedMedcard) {
        ObservationApiFactory.deletePulseById(id)
                .success(function (data) {
                    $scope.$emit('observationChanged', selectedMedcard);
                    $scope.toggleDeletePulse();
                });

    };

    $scope.addPressure = function (selectedMedcard, date, systolic, diastolic) {
        ObservationApiFactory.addPressureById(selectedMedcard.id, {date: date, top: systolic, lower: diastolic})
                .success(function (data) {
                    $scope.$emit('observationChanged', selectedMedcard);
                    $scope.togglePressureForm();
                });
    };

    $scope.deletePressure = function (id, selectedMedcard) {
        ObservationApiFactory.deletePressure(id)
                .success(function (data) {
                    $scope.$emit('observationChanged', selectedMedcard);
                    $scope.toggleDeletePressure();
                });
    };

}
;