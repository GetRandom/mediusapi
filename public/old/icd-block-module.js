function IcdBlockModule() {
    var allDiagnosisGetter = $.Deferred();
    var allDiagnosisGetterPromise = allDiagnosisGetter.pipe(med.icd.block);

    this.start = function () {
        $("#search").keyup(function (event) {
            if (event.keyCode === 13) {
                $("#search").click(function (event) {
                    window.location.href = '/icd/search/' + event.target.value;
                });
                $("#search").click();
            }
        });

        var str = window.location.href;
        allDiagnosisGetter.resolve(str.substring(str.lastIndexOf('/') + 1));
        $("#diagnosisList").css({"visibility": "hidden"});
        staticContentOn();
    };

    var successIcdGetter = function (result) {
        $('#diagnosisList').append(getIcdDiagnosisHtml(result));
        $('.preloaderbg').hide();
        $("#diagnosisList").css({"visibility": "visible"});
        staticContentOff();
    };

    allDiagnosisGetterPromise.done(successIcdGetter);
}
;
window.icdBlockModule = new IcdBlockModule();