function IcdClassModule() {
    var allBlocksGetter = $.Deferred();
    var allBlocksGetterPromise = allBlocksGetter.pipe(med.icd.class);

    this.start = function() {
        $("#search").keyup(function (event) {
            if (event.keyCode === 13) {
                $("#search").click(function (event) {
                    window.location.href = '/icd/search/' + event.target.value;
                });
                $("#search").click();
            }
        });
        
        var str = window.location.href;
        allBlocksGetter.resolve(str.substring(str.lastIndexOf('/') + 1));
        $("#blocksList").css({"visibility": "hidden"});
        staticContentOn();
    };

    var successIcdGetter = function(result) {
        $('#blocksList').append(getIcdBlocksHtml(result));
        $('.preloaderbg').hide();
        $("#blocksList").css({"visibility": "visible"});
        staticContentOff();
    };

    allBlocksGetterPromise.done(successIcdGetter);
}
;
window.icdClassModule = new IcdClassModule();