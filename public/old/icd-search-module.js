function IcdSearchModule() {
    var allResultsGetter = $.Deferred();
    var allResultsGetterPromise = allResultsGetter.pipe(med.icd.search);

    this.start = function() {
        $("#search").keyup(function (event) {
            if (event.keyCode === 13) {
                $("#search").click(function (event) {
                    window.location.href = '/icd/search/' + event.target.value;
                });
                $("#search").click();
            }
        });
        
        var str = window.location.href;
        allResultsGetter.resolve(str.substring(str.lastIndexOf('/') + 1));
    };

    var successIcdGetter = function(data) {
        $('#classesList').append(getIcdClassesHtml(data.class));
        $('#blocksList').append(getIcdBlocksHtml(data.block));
        $('#diagnosisList').append(getIcdDiagnosisHtml(data.diagnosis));
    };

    allResultsGetterPromise.done(successIcdGetter);
}
;
window.icdSearchModule = new IcdSearchModule();