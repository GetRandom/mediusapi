var med = {
    /** Staff module **/
    staff: {
        /** Auth Controller **/
        auth: {
            login: function (login, password) {
                return $.post('/api/auth/login', {'login': login, 'password': password});
            },
            logout: function () {
                return $.get('/api/auth/logout');
            }
        },
        /** Staff Controller **/
        staff: {
            /* Staff actions */
            view: function (id) {
                return $.get('/api/staff/view/' + id);
            },
            all: function (offset, count) {
                return $.get('/api/staff/all?offset=' + offset + '&count=' + count);
            },
            add: function (staff) {
                return $.post('/api/staff/add', staff);
            },
            update: function (staff) {
                return $.post('/api/staff/update', staff);
            },
            remove: function (id) {
                return $.post('/api/staff/remove', {'id': id});
            }
        },
        /** Digest Controller **/
        digest: {
            /* Digest Addition methods */
            addPost: function (title) {
                return $.post('/api/digest/add-post', {'title': title});
            },
            addSeparation: function (title) {
                return $.post('/api/digest/add-separation', {'title': title});
            },
            addWorkplace: function (title) {
                return $.post('/api/digest/add-workplace', {'title': title});
            },
            /* Digest Getters methods */
            getPosts: function () {
                return $.get('/api/digest/get-posts');
            },
            getSeparations: function () {
                return $.get('/api/digest/get-separations');
            },
            getWorkplaces: function () {
                return $.get('/api/digest/get-workplaces');
            },
            getRoles: function () {
                return $.get('/api/digest/get-roles');
            },
            /* Digest Remove methods */
            removePost: function (id) {
                return $.post('/api/digest/remove-post', {'id': id});
            },
            removeSeparation: function (id) {
                return $.post('/api/digest/remove-separation', {'id': id});
            },
            removeWorkplace: function (id) {
                return $.post('/api/digest/remove-workplace', {'id': id});
            }
        }
    },
    
    /** ICD module **/
    icd: {
        all: function () {
            return $.get('/api/icd/all');
        },
        class: function (id) {
            return $.get('/api/icd/class/' + id);
        },
        block: function (id) {
            return $.get('/api/icd/block/' + id);
        },
        search: function (id) {
            return $.get('/api/icd/search/' + id);
        }
    }
};

