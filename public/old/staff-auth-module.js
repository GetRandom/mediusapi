function StaffAuthModule() {
    var loginDeferred = $.Deferred();
    var loginPromise = loginDeferred.pipe(med.staff.auth.login);

    this.start = function() {
        $('#loginForm').submit(submitHandler);
    };
    var submitHandler = function(eventObject) {
        var data = formToJson($(eventObject.target));
        loginDeferred.resolve(data.login, data.password);
        return false;
    };

    var successLoginHandler = function(result) {
        var roles = result.data;
        for (var i = 0; i < roles.length; i++) {
            if (roles[i] === 'administrator') {
                window.location = '/staff';
            }
            else {
                window.location = '/medcard';
            }
        }
        
    };
    var failLoginHandler = function(result) {
        window.location.reload();
    };
    
    loginPromise.done(successLoginHandler);
    loginPromise.fail(failLoginHandler);
}
;
window.staffAuthModule = new StaffAuthModule();