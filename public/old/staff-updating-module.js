function StaffUpdatingModule() {
    var workplaceGetting = new $.Deferred();
    var separationGetting = new $.Deferred();
    var postGetting = new $.Deferred();
    var roleGetting = new $.Deferred();
    var viewStaff = new $.Deferred();
    var staffUpdating = new $.Deferred();
    var staffRemoving = new $.Deferred();

    var workplaceGettingPromise = workplaceGetting.pipe(med.staff.digest.getWorkplaces);
    var separationGettingPromise = separationGetting.pipe(med.staff.digest.getSeparations);
    var postGettingPromise = postGetting.pipe(med.staff.digest.getPosts);
    var roleGettingPromise = roleGetting.pipe(med.staff.digest.getRoles);
    var viewStaffPromise = viewStaff.pipe(med.staff.staff.view);
    var staffUpdatingPromise = staffUpdating.pipe(med.staff.staff.update);
    var staffRemovingPromise = staffRemoving.pipe(med.staff.staff.remove);

    this.start = function() {
        workplaceGetting.resolve();
        separationGetting.resolve();
        postGetting.resolve();
        roleGetting.resolve();
    };

    // Common handlers
    var failRequestHandler = function(result) {
        console.log(result);
    };
    var alwaysRequestHandler = function() {
        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        };
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        // May be extrated to route module
        var str = window.location.href;
        viewStaff.resolve(str.substring(str.lastIndexOf('/') + 1));
    };
    var submitHandler = function(eventObject) {
        if (true) { // Validation result will be here
            staffUpdating.resolve(formToJson($(eventObject.target)));
        }
        else {
            staffUpdating.reject('some errorMsg');
        }
        return false;
    };
    var removeButtonHandler = function(eventObject) {
        if (confirm('Удалить сотрудника?')) {
            staffRemoving.resolve(eventObject.target.value);
        }
        return false;
    };
    var successPassGenerationHandler = function(result) {
        $('#pass3')[0].value = result.password;
    };
    var passGenButtonHandler = function(eventObject) {
        $.get('/api/staff/pass-gen', successPassGenerationHandler);
        return false;
    };

    // Addition staff
    var successStaffUpdatingHandler = function(result) {
        alert('[OK]');
        window.location.reload();
    };
    var successStaffRemovingHandler = function() {
        window.location = '/staff';
    };

    // Get digests
    var successWorkplaceGettingHandler = function(result) {
        $('#workplaces_id').append(optionListFilling(result.data));
    };
    var successSeparationGettingHandler = function(result) {
        $('#separations_id').append(optionListFilling(result.data));
    };
    var successPostGettingHandler = function(result) {
        $('#posts_id').append(optionListFilling(result.data));
    };
    var successRoleGettingHandler = function(result) {
        $('#roles_id').append(optionListFilling(result.data));
    };
    var allDigestsReceivedHandler = function() {
        $("#jForm").submit(submitHandler);
        $('#remove').click(removeButtonHandler);
        $('#generate').click(passGenButtonHandler);

        $("form").css({"visibility": "visible"});

        enabledInputs();
        $('.preloaderbg').hide();
    };
    var successViewStaffHandler = function(result) {
        staffFormFilling($('#jForm')[0], result.data);

        $("#workplaces_id").trigger("chosen:updated");
        $("#separations_id").trigger("chosen:updated");
        $("#posts_id").trigger("chosen:updated");
        $("#roles_id").trigger("chosen:updated");
    };


    workplaceGettingPromise.done(successWorkplaceGettingHandler);
    separationGettingPromise.done(successSeparationGettingHandler);
    postGettingPromise.done(successPostGettingHandler);
    roleGettingPromise.done(successRoleGettingHandler);

    var allDigestsReceivedPromise = $.when(
            workplaceGettingPromise, separationGettingPromise,
            postGettingPromise, roleGettingPromise
            );

    allDigestsReceivedPromise.done(allDigestsReceivedHandler);
    allDigestsReceivedPromise.fail(failRequestHandler);
    allDigestsReceivedPromise.always(alwaysRequestHandler);

    viewStaffPromise.done(successViewStaffHandler);
    viewStaffPromise.fail(failRequestHandler);

    staffUpdatingPromise.done(successStaffUpdatingHandler);
    staffUpdatingPromise.fail(failRequestHandler);

    staffRemovingPromise.done(successStaffRemovingHandler);
    staffRemovingPromise.fail(failRequestHandler);
}
;
window.staffUpdatingModule = new StaffUpdatingModule();